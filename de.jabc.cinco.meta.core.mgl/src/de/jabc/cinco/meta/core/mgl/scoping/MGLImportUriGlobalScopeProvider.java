package de.jabc.cinco.meta.core.mgl.scoping;

import java.util.LinkedHashSet;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider;

import de.jabc.cinco.meta.core.utils.PathValidator;
import mgl.Import;
import mgl.MGLModel;

public class MGLImportUriGlobalScopeProvider extends ImportUriGlobalScopeProvider {

	@Override
	protected LinkedHashSet<URI> getImportedUris(Resource resource) {
		LinkedHashSet<URI> uris = super.getImportedUris(resource);
		
		for (EObject o : resource.getContents()) {
			if (o instanceof MGLModel) {
				MGLModel gm = (MGLModel) o;
				for (Import i : gm.getImports()) {
					try {
					String retVal = PathValidator.checkPath(gm, i.getImportURI());
					if (retVal != null && !retVal.isEmpty())
						continue;
					URI uri = PathValidator.getURIForString(gm, i.getImportURI());
					uris.add(uri);
					}catch(Exception e) {
						
					}
				}
			}
		}
		return uris;
	}
	
}
