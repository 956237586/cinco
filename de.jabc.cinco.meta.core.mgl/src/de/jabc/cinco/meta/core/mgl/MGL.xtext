grammar de.jabc.cinco.meta.core.mgl.MGL with org.eclipse.xtext.common.Terminals

import "http://www.jabc.de/cinco/meta/core/mgl"
import "http://www.eclipse.org/emf/2002/Ecore" as ecore

MGLModel returns MGLModel:
	(imports+=Import)*
	(annotations+=Annotation)*
	'id' package=QName
	'stylePath' stylePath=STRING
		 ((graphModels+=GraphModel)
		  |(nodes+=Node)
		  |(edges+=Edge)
		  |(nodes+=NodeContainer)
		  |(types+=Type))*
;

GraphModel returns GraphModel:
	
	//(includes+=Include)*
	(annotations+=Annotation)*
	(isAbstract?='abstract' )? 'graphModel'
	name=EString ('extends' (imprt=[Import|QName]'::')?extends=[GraphModel|EString])?
	'{'
		('iconPath' iconPath=EString)?
		('diagramExtension' fileExtension=EString)?
		('containableElements' '(' ((containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard) ( "," (containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard))*) ')' )?
		(attributes+=Attribute)*
		//('submodels' '(' submodels+=[GraphModel|EString] ( "," submodels+=[GraphModel|EString])* ')' )?
	'}'
;

Include returns Import:
	{Import}
	'include' importURI=STRING 'as' name=ID
;



URI:
	EString
;


Information returns Information:
	ModelCheckInformation | DataFlowInformation;

QName:
	(ID | ANY_OTHER)+(=>'.'(ID|ANY_OTHER)+)*
;

EString returns ecore::EString:
	STRING | ID;
	
EInt returns ecore::EInt:
	'-'? INT;
	
Attribute returns Attribute:
	PrimitiveAttribute|ComplexAttribute
	 
;

PrimitiveAttribute returns PrimitiveAttribute:
	(annotations+=Annotation)*
	(notChangeable?='final')? (unique?='unique')? 'attr' type=EDataTypeType 'as' name=EString ('[' lowerBound=EInt (',' ((upperBound=BoundValue)))? ']')?  (':=' defaultValue=EString)?
;

ComplexAttribute returns ComplexAttribute:
	(annotations+=Annotation)*
	(notChangeable?='final')? (unique?='unique')? (override?='override')? 'attr' type=[Type|EString] 'as' name=EString ('[' lowerBound=EInt (',' ((upperBound=BoundValue)))? ']')?  (':=' defaultValue=EString)?
;

enum EDataTypeType returns EDataTypeType:

	 EString 
	 |EChar
	 |EFloat
	 |EBoolean
	 |EDouble
	 |EInt
	 |ELong
	 |EBigInteger
	 |EBigDecimal
	 |EByte
	 |EShort
	 |EDate

;



GraphicalModelElement:
	Edge|Node|NodeContainer
	
	
;

Map:
	'EMap''<'QName','QName'>'
;

Edge returns Edge:
	{Edge}
	(annotations+=Annotation)*
	(isAbstract?='abstract' )?'edge'
	name=EString ('extends' (imprt=[Import|QName]'::')?extends=[Edge|EString])?
	('{'
		(
			((attributes+=Attribute (attributes+=Attribute)*)?)
			& ('style' usedStyle=ID ('(' styleParameters+=EString (',' styleParameters+=EString)* ')')? )?
		)
	'}')?
;

Node returns Node:
	{Node}
	(annotations+=Annotation)*
	(isAbstract?='abstract' )?'node'
	name=EString ('extends' (imprt=[Import|QName]'::')?extends=[Node|EString])?
	'{'
		(
			(attributes+=Attribute)*
			& ('style' usedStyle=ID ('(' styleParameters+=EString (',' styleParameters+=EString)* ')')? )?
			& (primeReference=(ReferencedEClass|ReferencedModelElement))?
			& ('incomingEdges' '(' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard) (',' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard))*')')?
			& ('outgoingEdges' '(' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard) (',' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard))*')')?
		)
	'}'
;

Annotation:
	{Annotation}
	'@'name=EString('('value+=EString (',' value+=EString)* ')')?
;

OrContainmentConstraint returns OrConstraint:
	negative?='not''<'constraints+=GraphicalElementContainment(',' constraints+=GraphicalElementContainment)*'>'
;

NodeContainer returns NodeContainer:
	{NodeContainer}
	(annotations+=Annotation)*
	(isAbstract?='abstract' )? 'container'
	name=EString ('extends' (imprt=[Import|QName]'::')?extends=[NodeContainer|EString])?
	'{'
		(
			(attributes+=Attribute)*
			& ('style' usedStyle=ID ('(' styleParameters+=EString (',' styleParameters+=EString)* ')')? )?
			& (primeReference=(ReferencedEClass|ReferencedModelElement))?
			& ('containableElements' '(' ((containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard) ( "," (containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard))*) ')' )?
			& ('incomingEdges' '(' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard) (',' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard))*')')?
			& ('outgoingEdges' '(' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard) (',' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard))*')')?
		)
		
	'}'
;

OutgoingEdgeElementConnection returns OutgoingEdgeElementConnection:
	{OutgoingEdgeElementConnection}
	((referencedImport=[Import]'::')? connectingEdges+=[Edge|QName]|(referencedImport=[Import]'::')? '{'connectingEdges+=[Edge|QName](','connectingEdges+=[Edge|QName])*'}')('['lowerBound=EInt ',' ((upperBound=BoundValue))']')?
;

IncomingEdgeElementConnection returns IncomingEdgeElementConnection:
	{IncomingEdgeElementConnection}
	((referencedImport=[Import]'::')? connectingEdges+=[Edge|QName]|(referencedImport=[Import]'::')? '{'connectingEdges+=[Edge|QName](','connectingEdges+=[Edge|QName])*'}')('['lowerBound=EInt ',' ((upperBound=BoundValue))']')?
;

GraphicalElementContainment returns GraphicalElementContainment:
	{GraphicalElementContainment}
	(((referencedImport=[Import]'::')? types+=[Node])|((referencedImport=[Import]'::')?'{'(types+=[Node])(','(types+=[Node]) )*'}'))('['lowerBound=EInt ','(upperBound=BoundValue) ']')?
;

Import returns Import:
	{Import}
(stealth?='stealth')? 'import' (external?='external')? importURI=STRING 'as' name=ID
;

BoundValue returns ecore::EInt:
	'*'|EInt

;

EdgeDirection returns EdgeDirection:
	//'EdgeDirection' /* TODO: implement this rule and an appropriate IValueConverter */;
		'Undirected'|
		'TargetDirected'|
		'SourceDirected'|
		'Bidirected'
	;
	

ModelCheckInformation returns ModelCheckInformation:
	{ModelCheckInformation}
	'ModelCheckInformation'
	'{'
		('extends' extends=[ModelCheckInformation|EString])?
	'}';

DataFlowInformation returns DataFlowInformation:
	{DataFlowInformation}
	'DataFlowInformation'
	'{'
		('extends' extends=[DataFlowInformation|EString])?
	'}';

Enum returns Enumeration:
	{Enumeration}
	(annotations+=Annotation)*
	'enum' name=EString '{'
							literals+=EString 
							(literals+=EString)*
						'}'
;

Type:
	Enum|UserDefinedType
;

UserDefinedType returns UserDefinedType:
	{UserDefinedType}
	(annotations+=Annotation)*
	(isAbstract?='abstract')? 'type' name=EString ('extends' (imprt=[Import|QName]'::')?extends=[UserDefinedType|EString])?'{' 
			(attributes+=Attribute)*
	'}'
;

Wildcard returns Wildcard:
	(selfWildcard?='*'|(referencedImport=[Import]'::''*'))('['lowerBound=EInt ','(upperBound=BoundValue) ']')?
;

ReferencedType returns ReferencedType:
	ReferencedEClass|ReferencedModelElement
;

ReferencedEClass returns ReferencedEClass:
	(annotations+=Annotation)*
	'prime' imprt=[Import|ID]'.'type=[ecore::EClass|ID] 'as' name=EString
	(copiedAttributes+=ReferencedEStructuralFeature)*
;

ReferencedModelElement returns ReferencedModelElement:
	(annotations+=Annotation)*
	'prime' (local?='this'|imprt=[Import|QName])'::'type=[ModelElement|QName] 'as' name=EString
	(copiedAttributes+=ReferencedMGLAttribute)*
;

ReferencedAttribute returns ReferencedAttribute:
	ReferencedMGLAttribute| ReferencedEStructuralFeature
;
ReferencedEStructuralFeature returns ReferencedEStructuralFeature:
		(parameter?='primeparam'|'primeattr') feature=[ecore::EStructuralFeature|QName] 'as' name=EString
;
ReferencedMGLAttribute returns ReferencedMGLAttribute:
	'primeattr' feature=[Attribute|QName] 'as' name=EString
;
