package de.jabc.cinco.meta.core.mgl.generator

import org.eclipse.core.internal.runtime.InternalPlatform
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.pde.core.project.IBundleProjectService
import productDefinition.CincoProduct
import java.util.List

class GenModelDescription{	
	String ecorePath
	Resource resource
	URI resourceUri
	String filePath
	String projectName
	Path projectPath
	CincoProduct model
	EPackage ePackage
	String projectID
	GenModel genModel
	List<Resource> externalGenmodels

	new(EPackage ePackage, CincoProduct cp, List<Resource> externalGenmodels){
		this.model = cp
		ecorePath = model.name + ".ecore".toFirstUpper
		this.ePackage = ePackage
		resource = model.eResource
		resourceUri = resource.URI
		filePath = resourceUri.toPlatformString(true)
		val iFile = ResourcesPlugin.workspace.root.getFile(new Path(filePath))
		projectName = iFile.project.name
		this.externalGenmodels = externalGenmodels
		
		projectPath = new Path(projectName)
		
		var bc = InternalPlatform::getDefault().getBundleContext();
		var ref = bc.getServiceReference(IBundleProjectService.name);
		var service = bc.getService(ref) as IBundleProjectService
		var bpd = service.getDescription(iFile.project);
		projectID = bpd.symbolicName
		bc.ungetService(ref);
	}
	
	def getEcorePath(){ecorePath}
	def getResource(){resource}
	def getResourceUri(){resourceUri}
	def getFilePath(){filePath}
	def getProjectName(){projectName}
	def getProjectPath(){projectPath}
	def getModel(){model}
	def getEPackage(){ePackage}
	def getProjectID(){projectID}
	def getExternalGenmodels(){externalGenmodels}
	
	def setGenModel(GenModel gm){
		this.genModel = gm
	}
	def getGenModel(){genModel}
}