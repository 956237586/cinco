# How to build a Linux icon

- Linux requires application icons in the `.xpm` format

- To build a `.xpm` file, you'll need [Gimp](https://www.gimp.org)



## Instructions

1. Import the largest icon into your canvas

2. Go to "File" > "Export"

3. Choose `icon_linux.xpm` as the file name and click "Export"

4. Set the alpha threshold to 200

5. Click "Export"
