package de.jabc.cinco.meta.core.jabcproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.UUID;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import mgl.MGLModel;

public class TransEM4SIBGenerator implements IRunnableWithProgress {

	private File projectPath =null;
	private IProject project;
	private MGLModel mglModel;
	private GeneratorUtils generatorUtils;
	
	public TransEM4SIBGenerator(IProject project,MGLModel mglModel){
		this.project = project;
		this.projectPath = project.getLocation().makeAbsolute().toFile();
		this.mglModel = mglModel;
		this.generatorUtils = GeneratorUtils.getInstance();
	}
	
	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {
		try{
			if(!isJABC4Project(projectPath)){
				generateJABC4Project(projectPath);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new InvocationTargetException(e);
		}
		try {
			project.refreshLocal(IProject.DEPTH_ONE, monitor);
		} catch (Exception e) {
			throw new InvocationTargetException(e);
		}
	}
	
	private void generateJABC4Project(File projectPath) throws FileNotFoundException, IOException {
		File projectFile = new File(projectPath +File.separator+"jabc.project");
		if(!projectFile.exists()){
			Properties jABC4Project = new Properties();
		
			jABC4Project.setProperty("ID", UUID.randomUUID().toString());
			jABC4Project.setProperty("jabc.project.sibpath.0","<classpath>");
			jABC4Project.setProperty("jabc.project.classpath.0", File.separator+"bin"+ File.separator);
			jABC4Project.setProperty("jabc.project.name",generatorUtils.getFileName(mglModel));
			jABC4Project.setProperty("jabc.project.definition","1.0");
			jABC4Project.setProperty("transem.qualified.package",getEPackageName(mglModel));
			jABC4Project.setProperty("transem.codegenerator.targetDir","src-gen/");
			jABC4Project.setProperty("transem.codegenerator.sourceDir","slg/");
			jABC4Project.setProperty("transem.SIBDir","slg-gen/");
			jABC4Project.setProperty("transem.other.ePackage.0","org.eclipse.graphiti.mm.algorithms.impl.AlgorithmsPackageImpl");
			jABC4Project.setProperty("transem.other.ePackage.1","org.eclipse.graphiti.mm.pictograms.impl.PictogramsPackageImpl");
			jABC4Project.store(new FileOutputStream(projectFile), "Saving jABC Project");
		}
	}

	private String getEPackageName(MGLModel mglModel2) {
		String mglName = firstUpper(generatorUtils.getFileName(mglModel2).toLowerCase());
		String mglPackage = ((MGLModel)mglModel2).getPackage();
		String mglNameLower = generatorUtils.getFileName(mglModel2).toLowerCase();
		return mglPackage.concat(".").concat(mglNameLower).concat(".").concat(mglName).concat("Package");
	}

	private boolean isJABC4Project(File projectPath){
		return new File(projectPath.getAbsolutePath()+File.separator+"jabc.project").exists();
	}
	
	private static String firstUpper(String string) {
		String fuString = "";
		if (string == null)
			return null;
		switch (string.length()) {
		case 0:
			return fuString;
		case 1:
			return string.toUpperCase();
		default:
			return string.substring(0, 1).toUpperCase()
					.concat(string.substring(1, string.length()));
		}
	}

}
