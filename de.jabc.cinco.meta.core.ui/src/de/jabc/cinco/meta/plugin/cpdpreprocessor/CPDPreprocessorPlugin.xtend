package de.jabc.cinco.meta.plugin.cpdpreprocessor

import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin
import java.util.List
import mgl.MGLModel
import mgl.MglFactory
import mgl.ModelElement
import org.eclipse.core.resources.IProject
import productDefinition.Annotation
import productDefinition.CincoProduct

class CPDPreprocessorPlugin implements ICPDMetaPlugin {
	
	new() {
		// Intentionally left blank
	}

	override executeCPDMetaPlugin(List<Annotation> cpdAnnotations,
	                              List<MGLModel> generatedMGLs,
	                              List<MGLModel> allMGLs,
	                              CincoProduct cpd,
	                              IProject mainProject) {
		
		val prepAnnot = cpd.annotations.filter[name == "preprocessor"].head
		if(prepAnnot === null) return;
		
		val mode = prepAnnot.value.head

		allMGLs.forEach[
			(nodes + edges + types + graphModels).map[annotations].flatten.filter[name == "preprocess"].map[
				if(value.head == mode) {
					println('''AAAAAAAAAAAAAAAAAAAAAAAAAA «(parent as ModelElement).name»: «value.get(1)»''')
					val newAnnotation = MglFactory.eINSTANCE.createAnnotation()
					newAnnotation.name = value.get(1)
					value.subList(2, value.size).forEach[
						newAnnotation.value.add(it)
					]
					parent -> newAnnotation
				}
			].toSet().filter[it !== null].forEach[ newAnnotationPair |
				newAnnotationPair.key.annotations.add(newAnnotationPair.value)
			]
		]
	}
}
