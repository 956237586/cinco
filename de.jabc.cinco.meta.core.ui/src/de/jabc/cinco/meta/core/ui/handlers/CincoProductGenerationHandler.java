package de.jabc.cinco.meta.core.ui.handlers;

import static de.jabc.cinco.meta.core.ui.utils.Stopwatch.TimeUnit.MILLISECOND;
import static de.jabc.cinco.meta.core.utils.messages.CincoMessageHandler.showQuestion;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.eclipse.core.resources.IResource.FORCE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.xtext.xbase.lib.Pair;
import org.jooq.lambda.tuple.Tuple2;

import com.google.common.base.Joiner;

import de.jabc.cinco.meta.core.BundleRegistry;
import de.jabc.cinco.meta.core.mgl.MGLEPackageRegistry;
import de.jabc.cinco.meta.core.mgl.generator.MGLGenerator;
import de.jabc.cinco.meta.core.pluginregistry.PluginRegistry;
import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;
import de.jabc.cinco.meta.core.ui.templates.ActivatorTemplate;
import de.jabc.cinco.meta.core.ui.templates.DefaultPerspectiveContent;
import de.jabc.cinco.meta.core.ui.templates.NewProjectWizardGenerator;
import de.jabc.cinco.meta.core.ui.utils.DirectedGraph;
import de.jabc.cinco.meta.core.ui.utils.Stopwatch;
import de.jabc.cinco.meta.core.utils.CincoProperties;
import de.jabc.cinco.meta.core.utils.CincoUtil;
import de.jabc.cinco.meta.core.utils.MGLUtil;
import de.jabc.cinco.meta.core.utils.PathValidator;
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.ConcurrentWorkload;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.core.utils.job.Workload;
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension;
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;
import de.jabc.cinco.meta.util.xapi.FileExtension;
import mgl.MGLModel;
import productDefinition.CincoProduct;
import productDefinition.MGLDescriptor;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
@SuppressWarnings("restriction")
public class CincoProductGenerationHandler extends AbstractHandler {
	
	private FileExtension   fileExtension  = new FileExtension();
	private GeneratorUtils  generatorUtils = GeneratorUtils.getInstance();

	private ICommandService commandService;
	private ExecutionEvent  event;
	private CompoundJob     job;
	
	private IProject        cpdProject;
	private IFile           cpdFile;
	private CincoProduct    cpd;
	private List<MGLFile>   allMGLFiles;
	private List<MGLFile>   generateMGLFiles;
	
	private Stopwatch       stopwatch;
	private boolean         pyroOnly                 = false;
	private boolean         headless                 = false;
	private long            lastGenerationTimestamp  = 0;
	private Boolean         autoBuildWasEnabled      = null;
	private boolean         atLeastOneFileChanged    = false;
	
	private static boolean  forceNextGeneration      = false;
	private static boolean  didGenerateAfterRestart  = false;
	private static boolean  lastGenerationDidSucceed = false;
	
	/**
	 * The constructor.
	 */
	public CincoProductGenerationHandler() {
		// Intentionally left blank
	}
	
	public void pyroOnly() {
		pyroOnly = true;
	}

	public void headless() {
		headless = true;
	}
	
	public static void forceNextGeneration() {
		forceNextGeneration = true;
	}
	
	/**
	 * The command has been executed, so extract the needed information from the
	 * application context and run the build job.
	 */
	synchronized public Object execute(ExecutionEvent executionEvent) throws ExecutionException {
		
		event = executionEvent;
		job = JobFactory.job("Generate Cinco Product");
		stopwatch = new Stopwatch(MILLISECOND);
				
		// PREPARING
		stopwatch.start("Preparing");
		readCPDFile();
		readCincoProperties();
		deleteFolders();
		readGenerationTimestamp();
		calculateMGLSets();
		generatorUtils.clearCaches();
		stopwatch.stop();
		
		if (!forceNextGeneration && didGenerateAfterRestart && lastGenerationDidSucceed && !atLeastOneFileChanged) {
			var answer = showQuestion(
				"No model changes!\n\nGenerate anyways? :-$",
				"Cinco Product Generator",
				headless
			);
			if (answer) {
				stopwatch.start("Recalculating MGL sets");
				forceNextGeneration();
				calculateMGLSets();
				stopwatch.stop();
			}
			else {
				return null;
			}
		}
		
		// INITIALIZATION
		workload(1, "Initializing")
			.task("Disabling auto-build", () -> disableAutoBuild());
		
		// CPD PROCESSING
		workload(1, "CPD processing")
			.task("Executing CPD meta plugins", () -> generateCPDMetaPlugins())
			.task("Recalculating MGL sets",     () -> calculateMGLSets());
		
		if (!pyroOnly) {
			
			// MGL PRE-PROCESSING
			workload(20, "MGL pre-processing")
				.task("Deleting previously generated resources", () -> deleteGeneratedResources())
				.taskForEach(
					()      -> generateMGLFiles.stream(),
					mglFile -> deleteGeneratedMGLResources(mglFile.mgl),
					mglFile -> "Deleting generated MGL resources: " + mglFile.file.getName()
				)
				.task("Generate Ecore Models", () -> generateEcoreModels());
			
			// MGL PROCESSING
			workload(10 * generateMGLFiles.size(), "MGL processing")
				.taskForEach(
					() -> generateMGLFiles.stream(),
					mglFile -> {
						var file = mglFile.file;
						generateCincoSIBs(file);
						generateFeatureProject(file);
						generateGratextModel(file);
					},
					mglFile -> mglFile.file.getName()
				);
			
			// MGL POST-PROCESSING
			workload(5, "MGL post-processing")
				.task("Generating Graphiti editor", () -> generateGraphitiEditor())
				.task("Generating product project", () -> generateProductProject());
			
			// BUILD PROJECT
			workload(15, "Building Cinco project")
				.task("Building project", () -> buildProject());

			// BUILD GRATEXT
			concurrentWorkload(50 + 10 * generateMGLFiles.size(), "Building Gratext")
				.taskForEach(
					()      -> generateMGLFiles.stream(),
					mglFile -> buildGratext(),
					mglFile -> mglFile.file.getName()
				);
			
			// GLOBAL PROCESSING
			workload(1, "Global processing")
				.task("Generating Activator", () -> generateActivator())
				.task("Generating project wizard", () -> generateProjectWizard());
		}
				
		// START JOBS
		job
			.consume(0)
				.task(() -> stopwatch.stop())
			.onFinished(() -> {
				writeGenerationTimestamp();
				forceNextGeneration = false;
				didGenerateAfterRestart = true;
				lastGenerationDidSucceed = true;
				stopwatch.printTable();
				restoreAutoBuild();
				System.out.println("Generation successful!");
			})
			.onCanceled(() -> {
				forceNextGeneration = false;
				lastGenerationDidSucceed = false;
				stopwatch.printTable();
				restoreAutoBuild();
				System.out.println("Generation canceled!");
			})
			.onFailed(() -> {
				forceNextGeneration = false;
				lastGenerationDidSucceed = false;
				stopwatch.printTable();
				restoreAutoBuild();
				System.out.println("Generation failed!");
			})
			.onFinishedShowMessage("Cinco Product generation finished successfully.")
			.onCanceledShowMessage("Cinco Product generation has been canceled.")
			.onFailedShowMessage("Cinco Product generation failed.")
			.schedule();
		
		pyroOnly = false;
		return null;
	}
	
	private Workload workload(int quota, String label) {
		var workload = job.consume(quota, label);
		workload.task(() -> stopwatch.start(label));
		workload.task(() -> System.out.println("Starting workload: " + label));
		return workload;
	}
	
	private ConcurrentWorkload concurrentWorkload(int quota, String label) {
		var workload = job.consumeConcurrent(quota, label);
		workload.task(() -> stopwatch.start(label));
		workload.task(() -> System.out.println("Starting concurrent workload: " + label));
		workload.setMaxThreads(CincoProperties.getMaxThreads());
		return workload;
	}
	
	private void generateEcoreModels() {
		// Additionally get imported external MGLs and
		// check if their generated sources exist.
		// If not, add it to the generation process.
		ResourceExtension resourceExtension = new ResourceExtension();
		HashMap<MGLModel, Pair<Resource, Resource>> importedExternalMGLs = new HashMap<MGLModel, Pair<Resource, Resource>>();
		generateMGLFiles
			.stream()
			.flatMap(mglFile -> mglFile.mgl.getImports().stream())
			.filter(imprt -> imprt.isExternal())
			.forEach(imprt -> {
				var mglResource    = fileExtension.getResource(URI.createURI(imprt.getImportURI()));
				var mglModel       = resourceExtension.getContent(mglResource, MGLModel.class);
				var uriSegments    = imprt.getImportURI().split("/");
				var baseFolder     = "";
				for (int i = 0; i < uriSegments.length - 2; i++) {
					baseFolder += uriSegments[i] + "/";
				}
				var genModelDirUri = URI.createURI(baseFolder + "src-gen/model/").toPlatformString(false);
				var modelDirUri    = URI.createURI(baseFolder + "model/").toPlatformString(false);
				var workspaceRoot  = ResourcesPlugin.getWorkspace().getRoot();
				var rootPath       = workspaceRoot.getFullPath();
				var genModelDir    = workspaceRoot.getFolder(rootPath.append(genModelDirUri));
				var modelDir       = workspaceRoot.getFolder(rootPath.append(modelDirUri));
				
				Resource ecoreResource = null;
				Resource genModel      = null;
				
				try {
					var genModelDirResources = genModelDir.members();
					for (var resourceCandidate : genModelDirResources) {
						if (resourceCandidate.getFileExtension().equals("ecore")) {
							ecoreResource = fileExtension.getResource(
								URI.createURI(resourceCandidate.getFullPath().toString())
							);
						}
						// Load the genmodel to ensure it is available in the EMF registry
						if (resourceCandidate.getFileExtension().equals("genmodel")) {
							genModel = fileExtension.getResource(
								URI.createURI(resourceCandidate.getFullPath().toString())
							);
						}
					}
					var modelDirResources = modelDir.members();
					for (var resourceCandidate : modelDirResources) {
						if (resourceCandidate.getFileExtension().equals("cpd")) {
							MGLUtil.mglModelCpdMap.put(mglModel, fileExtension.getContent(
								modelDir.getFile(resourceCandidate.getName()), CincoProduct.class
							));
						}
					}
				}
				catch (CoreException e) {
					e.printStackTrace();
				}
				
				if (ecoreResource != null && genModel != null) {
					importedExternalMGLs.put(mglModel, new Pair<Resource, Resource>(ecoreResource, genModel));						
				}
				else {
					throw new RuntimeException(
						"The ecore model of the external import " +
						imprt.getImportURI() + " cannot be found." +
						"Please make sure it has already been generated."
					);
				}
				
			});
		
		var allMGLsList = allMGLFiles.stream().map(mglFile -> mglFile.mgl).collect(toList());
		var generateMGLsSet = generateMGLFiles.stream().map(mglFile -> mglFile.mgl).collect(toSet());
		generatorUtils.allMGLs = generateMGLsSet;
		generatorUtils.cpd = cpd;
		
		new MGLGenerator().doGenerateEcoreModels(allMGLsList, generateMGLsSet, importedExternalMGLs, cpd);
	}

	/**
	 * Executes the CPD meta plugins
	 */
	private void generateCPDMetaPlugins() {
		var cpdMetaPluginGenerators = PluginRegistry
			.getInstance()
			.getPluginCPDGenerators()
			.stream()
			.sorted((l, r) -> {
				var priorityDiff = -(l.getPlugin().comparePriorityTo(r.getPlugin())); // Highest priority first
				if (priorityDiff == 0) {
					return l.getAnnotationName().compareToIgnoreCase(r.getAnnotationName()); // Lexicographical order
				}
				else {
					return priorityDiff;
				}
			})
			.map(a -> new Pair<>(a.getAnnotationName(), a.getPlugin()))
			.collect(toList());
		if (cpdMetaPluginGenerators == null || cpdMetaPluginGenerators.isEmpty()) {
			return;
		}
		var allAnnotations = cpd.getAnnotations();
		if (allAnnotations == null || allAnnotations.isEmpty()) {
			return;
		}
		var allMGLsList = allMGLFiles
			.stream()
			.map(mglFile -> mglFile.mgl)
			.collect(toList());
		var generateMGLsList = generateMGLFiles
			.stream()
			.map(mglFile -> mglFile.mgl)
			.collect(toList());
		for (var entry : cpdMetaPluginGenerators) {
			var cpdMetaPluginName = entry.getKey();
			var cpdMetaPlugin = entry.getValue();
			var relatedAnnotations = allAnnotations
				.stream()
				.filter(annotation -> annotation.getName().equals(cpdMetaPluginName))
				.collect(toList());
			if (relatedAnnotations != null && !relatedAnnotations.isEmpty()) {
				System.out.println(
					"Executing CPD meta plugin: " + cpdMetaPluginName +
					" (Priority: " + cpdMetaPlugin.getCPDMetaPluginPriority() + ")"
				);
				cpdMetaPlugin.executeCPDMetaPlugin(relatedAnnotations, generateMGLsList, allMGLsList, cpd, cpdProject);
			}
		}
	}

	private void readCPDFile() {
		commandService = PlatformUI.getWorkbench().getService(ICommandService.class);
		cpdFile = MGLSelectionListener.INSTANCE.getSelectedCPDFile();
		if (!(cpdFile instanceof IFile)) {
			throw new RuntimeException("No valid CPD file selected!");
		}
		cpdProject = cpdFile.getProject();
		cpd = fileExtension.getContent(cpdFile, CincoProduct.class, 0);
		MGLSelectionListener.INSTANCE.setCurrentCPD(cpd);
	}
	
	private void generateGraphitiEditor() {
		execute("de.jabc.cinco.meta.core.ge.style.generator.newgraphitigenerator");
	}
	
	private void generateCincoSIBs(IFile mglFile) {
		MGLSelectionListener.INSTANCE.putMGLFile(mglFile);
		execute("de.jabc.cinco.meta.core.jabcproject.commands.generateCincoSIBsCommand");
	}
	
	private void generateProductProject() {
		execute("cpd.handler.ui.generate");
	}
	
	private void generateFeatureProject(IFile mglFile) {
		MGLSelectionListener.INSTANCE.putMGLFile(mglFile);
		execute("de.jabc.cinco.meta.core.generatefeature");
	}
	
	private void generateGratextModel(IFile mglFile) {
		if (isGratextEnabled()) {
			MGLSelectionListener.INSTANCE.putMGLFile(mglFile);
			execute("de.jabc.cinco.meta.plugin.gratext.generategratext");
		}
	}
	
	private void buildProject() {
		try {
			cpdProject.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, null);
		}
		catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	private void buildGratext() {
		if (isGratextEnabled()) {
			execute("de.jabc.cinco.meta.plugin.gratext.buildgratext");
		}
	}
	
	private boolean isGratextEnabled() {
		return cpd
			.getAnnotations()
			.stream()
			.noneMatch(ann -> "disableGratext".equals(ann.getName()));
	}
	
	private void deleteFolders() {
		for (String folder : CincoProperties.getDeleteFolders()) {
			try {
				var resource = cpdProject.findMember(folder);
				if (resource != null) {
					resource.delete(FORCE, null);
				}
			}
			catch (CoreException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void deleteGeneratedResources() {
		if (generateMGLFiles == null || generateMGLFiles.isEmpty()) {
			return;
		}
		try {
			var resourcesGen = cpdProject.findMember("resources-gen/");
			if (resourcesGen != null) {
				resourcesGen.delete(org.eclipse.core.resources.IResource.FORCE, null);
			}
		}
		catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void deleteGeneratedMGLResources(MGLModel mgl) {
		try {
			var mglPackagePath = toPath(mgl.getPackage());
			var packageFolder = cpdProject.getFolder("src-gen/" + mglPackagePath);
			if (packageFolder.exists()) {
				packageFolder.delete(FORCE, null);
			}
		}
		catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}

	private void calculateMGLSets() {
		// Reset registries
		BundleRegistry.resetRegistry();
		MGLEPackageRegistry.resetRegistry();
		
		// Unsorted list of all MGLFiles 
		var mglFiles = cpd
			.getMgls()
			.stream()
			.map(mglDesc -> new MGLFile(mglDesc))
			.collect(toList());
		
		// Set initial flag, whether generation is required
		for (var mglFile : mglFiles) {
			if (!isGenerationRequired(mglFile) && !pyroOnly) {
				MGLEPackageRegistry.INSTANCE.addMGLEPackage(getEPackageForMGL(mglFile.file, cpdProject));
			}
		}
		
		// Build dependency graph and initial set of ignored MGLs
		var dependencyGraph = new DirectedGraph<MGLFile>();
		for (var mglFile : mglFiles) {
			var parents = mglFile
				.mgl
				.getImports()
				.stream()
				.filter(imprt -> !imprt.isStealth() && !imprt.isExternal())
				.map(imprt -> {
					var path = PathValidator.getRelativePath(imprt.getImportURI(), cpdProject);
					return mglFiles
						.stream()
						.filter(mgl -> mgl.path.equals(path))
						.findFirst()
						.orElse(null);
				})
				.filter(mgl -> mgl != null)
				.collect(toList());
			dependencyGraph
				.addNode(mglFile)
				.addParents(parents);
		}
		
		// Sort list of all MGLFiles by dependency
		allMGLFiles = dependencyGraph.getTopSortedContents();
		
		// MGLs must be generated, if any of their ancestors or descendants require generation
		generateMGLFiles = new ArrayList<>();
		var independentSubgraphs = dependencyGraph.getSubgraphs();
		for (var subgraph : independentSubgraphs) {
			var subgraphMGLFiles = subgraph.getContents();
			var subgraphContainsGenerationRequiredMGLFiles = subgraphMGLFiles
				.stream()
				.anyMatch(mglFile -> mglFile.generationRequired);
			if (subgraphContainsGenerationRequiredMGLFiles) {
				generateMGLFiles.addAll(subgraphMGLFiles);
			}
		}
		
	}
	
	private EPackage getEPackageForMGL(IFile mglFile, IProject project) {
		var ecoreName = cpd.getName() + ".ecore";
		var uri = URI.createPlatformResourceURI(ProjectCreator.getProjectSymbolicName(project) + "/src-gen/model/" + ecoreName, true);
		var resource = Resource.Factory.Registry.INSTANCE.getFactory(uri).createResource(uri);
		try {
			resource.load(null);
			return (EPackage) resource.getContents().get(0);
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to load ecore model: " + ecoreName, e);
		}
	}

	private String toPath(String e) {
		return e.replace('.', '/');
	}

	private void execute(String commandId) {
		try {
			commandService.getCommand(commandId).executeWithChecks(event);
		}
		catch (ExecutionException | NotDefinedException | NotEnabledException | NotHandledException e) {
			var mglFile = MGLSelectionListener.INSTANCE.getCurrentMGLFile();
			var fileName = mglFile == null ? cpdFile.getName() : mglFile.getName();
			throw new RuntimeException("Generation of " + fileName + " failed", e);
		}
	}
	
	private boolean isAutoBuild() {
		var workspace = ResourcesPlugin.getWorkspace();
		var desc = workspace.getDescription();
		return desc.isAutoBuilding();
	}
	
	private void setAutoBuild(boolean enable) throws CoreException {
		var workspace = ResourcesPlugin.getWorkspace();
		var desc = workspace.getDescription();
		desc.setAutoBuilding(enable);
		workspace.setDescription(desc);
	}

	private void disableAutoBuild() {
		if (autoBuildWasEnabled == null) {
			autoBuildWasEnabled = isAutoBuild();
		}
		try {
			setAutoBuild(false);
		}
		catch (CoreException e) {
			if (isAutoBuild()) {
				System.out.println("Failed to deactivate \"Build Automatically\".");
				e.printStackTrace();
			}
		}
	}

	private void restoreAutoBuild() {
		if (autoBuildWasEnabled == null) {
			return;
		}
		try {
			setAutoBuild(autoBuildWasEnabled);
		}
		catch (CoreException e) {
			if (autoBuildWasEnabled != isAutoBuild()) {
				System.err.println("Failed to restore state for \"Build Automatically\". Should be: " + autoBuildWasEnabled);
				e.printStackTrace();
			}
		}
		autoBuildWasEnabled = null;
	}

	private void generateProjectWizard() {
		if (generateMGLFiles == null || generateMGLFiles.isEmpty()) {
			return;
		}
		
		System.out.println("Generating Project Wizard");
		var pluginXMLFile = cpdProject.getFile("plugin.xml");
		var projectName   = cpdProject.getName();
		var cpdName       = cpd.getName();
		var cpdNameUpper  = cpdName.toUpperCase();
		
		var wizardExtensionCommentID      = "<!--@CincoGen PROJECT_WIZARD_"      + cpdNameUpper + "_WIZ -->";
		var navigatorExtensionCommentID   = "<!--@CincoGen PROJECT_WIZARD_"      + cpdNameUpper + "_NAV -->";
		var perspectiveExtensionCommentID = "<!--@CincoGen PROJECT_PERSPECTIVE_" + cpdNameUpper + "_PER -->";
		
		var wizardJavaCode = NewProjectWizardGenerator.generateWizardJavaCode(cpd, projectName);
		var newWizardXML   = NewProjectWizardGenerator.generateNewWizardXML(  cpd, projectName, wizardExtensionCommentID);
		var navigatorXML   = NewProjectWizardGenerator.generateNavigatorXML(  cpd, projectName, navigatorExtensionCommentID);
		var perspectiveXML = DefaultPerspectiveContent.generateXMLPerspective(cpd, projectName);

		var workspaceExtension = new WorkspaceExtension();
		var wizardPath = "src-gen/" + projectName.replace(".", "/") + "/";
		var wizardFile = cpdProject.getFile(wizardPath + cpdName + "ProjectWizard.java");		
		workspaceExtension.createFolders(cpdProject, workspaceExtension.toPath(wizardPath));
		
		try {
			workspaceExtension.create(wizardFile);
			fileExtension.writeContent(wizardFile, wizardJavaCode.toString());
		}
		catch (CoreException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		var pluginXMLLocation = pluginXMLFile.getLocation().toString();
		CincoUtil.addExtension(pluginXMLLocation, newWizardXML.toString(),   wizardExtensionCommentID,      projectName);
		CincoUtil.addExtension(pluginXMLLocation, navigatorXML.toString(),   navigatorExtensionCommentID,   projectName);
		CincoUtil.addExtension(pluginXMLLocation, perspectiveXML.toString(), perspectiveExtensionCommentID, projectName);
	}

	private void readCincoProperties() {
		CincoProperties.newInstance().load(cpdProject);
	}

	private void readGenerationTimestamp() {
		var file = getGenerationTimestampFile();
		if (!file.exists()) {
			lastGenerationTimestamp = 0;
			return;
		}
		try {
			var reader = new BufferedReader(new FileReader(file));
			var lines = reader.lines().collect(toList());
			reader.close();
			var contents = Joiner.on("\n").join(lines);
			lastGenerationTimestamp = Long.parseLong(contents);
		}
		catch (UncheckedIOException | IOException | NumberFormatException e) {
			e.printStackTrace();
		}
	}

	private void writeGenerationTimestamp() {
		lastGenerationTimestamp = System.currentTimeMillis();
		var file = getGenerationTimestampFile();
		file.delete();
		try {
			file.getParentFile().mkdirs();
			file.createNewFile();
			var writer = new FileWriter(file);
			writer.write(Long.toString(lastGenerationTimestamp));
			writer.flush();
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File getGenerationTimestampFile() {
		return cpdFile
			.getProject()
			.getLocation()
			.append("src-gen")
			.append("model")
			.append(cpdFile.getName() + ".generation-timestamp")
			.toFile();
	}
	
	private boolean hasChanged(IFile file) {
		var lastModifiedTimestamp = file.getLocation().toFile().lastModified(); // in milliseconds
		if (lastModifiedTimestamp > lastGenerationTimestamp) {
			return true;
		}
		return false;
	}
	
	private boolean isGenerationRequired(MGLFile mglFile) {
		if (forceNextGeneration) {
			System.out.println(mglFile.file.getName() + ": Generation required (Force generation)");
			mglFile.generationRequired = true;
			return true;
		}
		if (!didGenerateAfterRestart) {
			System.out.println(mglFile.file.getName() + ": Generation required (First generation after restart)");
			mglFile.generationRequired = true;
			return true;
		}
		if (!lastGenerationDidSucceed) {
			System.out.println(mglFile.file.getName() + ": Generation required (Last generation did fail)");
			mglFile.generationRequired = true;
			return true;
		}
		if (lastGenerationTimestamp <= 0) {
			System.out.println(mglFile.file.getName() + ": Generation required (Missing generation timestamp file)");
			mglFile.generationRequired = true;
			return true;
		}
		if (mglFile.descriptor.isForceGenerate()) {
			System.out.println(mglFile.file.getName() + ": Generation required (MGL '" + mglFile.file.getName() + "' is set to 'forceGenerate')");
			mglFile.generationRequired = true;
			return true;
		}
		if (mglFile.descriptor.isDontGenerate()) {
			System.out.println(mglFile.file.getName() + ": Generation not required (MGL '" + mglFile.file.getName() + "' is set to 'generateNOT')");
			mglFile.generationRequired = false;
			return false;
		}
		if (hasChanged(cpdFile)) {
			atLeastOneFileChanged = true;
			System.out.println(mglFile.file.getName() + ": Generation required (CPD '" + cpdFile.getName() + "' changed)");
			mglFile.generationRequired = true;
			return true;
		}
		if (hasChanged(mglFile.file)) {
			atLeastOneFileChanged = true;
			System.out.println(mglFile.file.getName() + ": Generation required (MGL '" + mglFile.file.getName() + "' changed)");
			mglFile.generationRequired = true;
			return true;
		}
		var stylePath = PathValidator.getRelativePath(mglFile.mgl.getStylePath(),cpdProject);
		var styleFile = cpdProject.getFile(stylePath);
		if (styleFile.exists() && hasChanged(styleFile)) {
			atLeastOneFileChanged = true;
			System.out.println(mglFile.file.getName() + ": Generation required (MSL '" + styleFile.getName() + "' changed)");
			mglFile.generationRequired = true;
			return true;
		}
		System.out.println(mglFile.file.getName() + ": Generation not required (Nothing changed)");
		mglFile.generationRequired = false;
		return false;
	}

	private void generateActivator() {
		var workspaceExtension   = new WorkspaceExtension();
		var activatorName        = "Activator";
		var activatorPackage     = cpdProject.getName();
		var activatorFQN         = activatorPackage + "." + activatorName;
		var activatorContents    = ActivatorTemplate.contents(activatorPackage).toString();
		var activatorPackagePath = "src-gen/" + activatorPackage.replace(".", "/") + "/";
		var activatorFilePath    = activatorPackagePath + activatorName + ".java";
		var activatorFile        = cpdProject.getFile(activatorFilePath);
		// Create activator file
		try {
			workspaceExtension.createFolders(cpdProject, workspaceExtension.toPath(activatorPackagePath));
			workspaceExtension.create(activatorFile);
			fileExtension.writeContent(activatorFile, activatorContents);
		}
		catch (CoreException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// Add activator to manifest
		ProjectCreator.addAttribute(
			cpdProject,
			null,
			new Tuple2<String, String>("Bundle-Activator", activatorFQN),
			new Tuple2<String, String>("Bundle-ActivationPolicy", "lazy")
		);
	}
	
	private class MGLFile {
		
		public final MGLDescriptor descriptor;
		public final String path;
		public final IFile file;
		public final MGLModel mgl;
		public boolean generationRequired;
		
		public MGLFile(MGLDescriptor mglDesc) {
			descriptor         = mglDesc;
			path               = PathValidator.getRelativePath(descriptor.getMglPath(),cpdProject);
			file               = cpdProject.getFile(path);
			mgl                = fileExtension.getContent(file, MGLModel.class);
			generationRequired = false;
		}
		
	}
	
}
