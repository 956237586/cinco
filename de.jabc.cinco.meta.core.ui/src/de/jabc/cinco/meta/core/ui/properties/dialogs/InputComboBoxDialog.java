package de.jabc.cinco.meta.core.ui.properties.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.GridLayout;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * Input dialog providing a combo box as input selection. 
 */
public class InputComboBoxDialog extends Dialog {

	private Object value;
	
	private Combo combo;
	private ComboViewer comboViewer;
	
	private IContentProvider cProvider = new ArrayContentProvider();
	private ILabelProvider lProvider = new LabelProvider();
	private List<?> input = new ArrayList<>();
	
	protected InputComboBoxDialog(Shell parentShell) {
		super(parentShell);
	}
	
	public InputComboBoxDialog(Shell parentShell, List<?> input) {
		super(parentShell);
		if (input != null) this.input = input;
	}
	
	/**
	 * Constructor for an InputDialog like dialog but with combo box as selection element.
	 * @param parentShell The parent shell
	 * @param cProvider Optional parameter, default is {@link ArrayContentProvider} 
	 * @param lProvider Optional parameter, default is {@link LabelProvider}
	 * @param input 
	 */
	public InputComboBoxDialog(Shell parentShell, IContentProvider cProvider, ILabelProvider lProvider, List<?> input) {
		super(parentShell);
		if (cProvider != null) this.cProvider = cProvider;
		if (lProvider != null) this.lProvider = lProvider;
		if (input != null) this.input = input;
	}
	
	public InputComboBoxDialog(Shell parentShell, IContentProvider cProvider, ILabelProvider lProvider, List<?> input, Object selectedElement) {
		super(parentShell);
		if (cProvider != null) this.cProvider = cProvider;
		if (lProvider != null) this.lProvider = lProvider;
		if (input != null) this.input = input;
		this.value = selectedElement;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite) super.createDialogArea(parent);
		comp.setLayoutData(new GridLayout(2, false));
		comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		Label label = new Label(comp, SWT.NONE);
		label.setText("Select value: ");
		combo = new Combo(comp, SWT.READ_ONLY);
		combo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		comboViewer = new ComboViewer(combo);
		
		comboViewer.setContentProvider(cProvider);
		comboViewer.setLabelProvider(lProvider);
		comboViewer.setInput(input);
		
		if (value != null) {
			comboViewer.setSelection(new StructuredSelection(value));
		}
		
		return comp;
	}

	public Object getValue() {
		return value;
	}
	
	@Override
	protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.OK_ID) {
            value = comboViewer.getStructuredSelection().getFirstElement();
        } else {
            value = null;
        }
        super.buttonPressed(buttonId);
    }
 
}
