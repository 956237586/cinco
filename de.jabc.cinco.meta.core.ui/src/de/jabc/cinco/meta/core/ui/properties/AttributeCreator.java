package de.jabc.cinco.meta.core.ui.properties;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Display;

import de.jabc.cinco.meta.core.ui.properties.dialogs.InputComboBoxDialog;

public class AttributeCreator {

	public static String createAttribute(EAttribute attr) {
		String retval = null;
		Dialog dialog = getDialog(attr);
		if (dialog.open() == Dialog.OK)
			return getValue(dialog);
		
		return retval;
	}

	public static String createAttribute(EAttribute attr, Object selectedObject) {
		String retval = null;
		Dialog dialog = getDialog(attr, selectedObject);
		if (dialog.open() == Dialog.OK)
			return getValue(dialog);
		
		return retval;
	}
	
	private static Dialog getDialog(EAttribute attr) {
		if (attr.getEType() instanceof EEnum) {
			 EList<EEnumLiteral> input = ((EEnum) attr.getEType()).getELiterals();
			 InputComboBoxDialog d = new InputComboBoxDialog(Display.getCurrent().getActiveShell(), input);
			 return d;
		} else {
			return new InputDialog(
					Display.getCurrent().getActiveShell(), 
					"New " + attr.getName(), 
					"Create a new " + attr.getName() + " value", 
					getInitialValue(attr), 
					getValidator(attr));
		}
	}
	
	private static Dialog getDialog(EAttribute attr,Object selectedObject) {
		if (attr.getEType() instanceof EEnum) {
			EList<EEnumLiteral> input = ((EEnum) attr.getEType()).getELiterals();
			InputComboBoxDialog d = new InputComboBoxDialog(Display.getCurrent().getActiveShell(),input);
			return d;
		} else {
			return new InputDialog(
				Display.getCurrent().getActiveShell(), 
				"New " + attr.getName(), 
				"Create a new " + attr.getName() + " value", 
				selectedObject.toString(),
				getValidator(attr));
		}
	}

	private static IInputValidator getValidator(final EAttribute attr) {
		return new IInputValidator() {
			
			@Override
			public String isValid(String newText) {
				try {
					EcoreUtil.createFromString(attr.getEAttributeType(), newText);
				} catch (Exception e) {
					return "Input: \"" + newText + "\" is not a valid " + attr.getEAttributeType().getName();
				}
				return null;
			}
		};
		
	}

	private static String getValue(Dialog dialog) {
		if (dialog instanceof InputDialog)
			return ((InputDialog) dialog).getValue();
		if (dialog instanceof InputComboBoxDialog)
			return ((InputComboBoxDialog) dialog).getValue().toString();
		else return null;
			
	}
	
	private static String getInitialValue(EAttribute attr) {
		EDataType eAttributeType = attr.getEAttributeType();
		switch (eAttributeType.getName()) {
		case "EInt":
			return "0";
		case "EEnum":
			return "";
		default:
			break;
		}
		return null;
	}
	
}
