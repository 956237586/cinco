package de.jabc.cinco.meta.core.ui.editor

import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import java.util.Set

class ResourceContributorRegistry {
	
	static val EXTENSION_ID = "de.jabc.cinco.meta.core.ui.ResourceContributor"
	
	static var Set<ResourceContributor> contributors
	
	static def getContributors() {
		contributors ?: (
			contributors = new WorkspaceExtension().getExtensions(EXTENSION_ID, ResourceContributor).toSet
		)
	}
}
