package de.jabc.cinco.meta.core.ui.utils

import java.util.HashSet
import java.util.LinkedList
import java.util.Set
import org.eclipse.xtend.lib.annotations.Accessors

/**
 * A directed graph, where each {@link DirectedGraph.Node Node} has references
 * to both its parents and its children. Graphs may be disconnected, empty or
 * cyclic.
 * <p>
 * Nodes will be compared using their {@link DirectedGraph.Node#getContent()
 * contents}. You cannot add more than one node with the same content. Instead,
 * the node with the same content will be modified.
 * <p>
 * To create a graph could use code like this:
 * <pre>
 * var graph = new DirectedGraph&ltInteger&gt();
 * graph
 *     .addNode(0)
 *     .addNode(1)
 *         .addParents(0)
 *         .addChildren(2, 3)
 *     .addNode(4)
 *         .addChildren(5, 6, 7)
 *     .addNode(8)
 *         .addParents(2, 7)
 *     .getGraph()
 *         .print()
 *     .getNode(8)
 *         .getAncestors()
 *         .stream()
 *         .forEach(node -> node.print());
 * </pre>
 * Result:
 * <pre>
 * DirectedGraph {
 *     0 -> {1},
 *     1 -> {2, 3},
 *     2 -> {8},
 *     3 -> {},
 *     4 -> {5, 6, 7},
 *     5 -> {},
 *     6 -> {},
 *     7 -> {8},
 *     8 -> {}
 * }
 * 7 -> {8}
 * 2 -> {8}
 * 4 -> {5, 6, 7}
 * 1 -> {2, 3}
 * 0 -> {1}
 * </pre>
 */
@Accessors(PUBLIC_GETTER)
class DirectedGraph<T> {
	
	/**
	 * A set of all nodes contained by this graph.
	 */
	val Set<Node<T>> nodes
	
	/**
	 * Creates a new empty directed graph.
	 */
	new () {
		nodes = new HashSet
	}
	
	/**
	 * Returns the node, that contains {@code content}.
	 * Returns {@code null}, if there is no such node.
	 */
	def getNode(T content) {
		nodes.findFirst[ node | node.content == content ]
	}
	
	/**
	 * Returns an existing node, that contains {@code content}.
	 * If there is no such node, a new node will be created and added to the
	 * graph.
	 */
	def addNode(T content) {
		var node = getNode(content)
		if (node === null) {
			node = new Node(content, this)
			nodes.add(node)
		}
		return node
	}
	
	/**
	 * Whether or not this graph contains a node with this {@code content}.
	 */
	def contains(T content) {
		nodes.exists[ node | node.content == content ]
	}
	
	/**
	 * Whether or not this graph contains nodes with all {@code contents}.
	 */
	def containsAll(T ... contents) {
		contents.forall[contains]
	}
	
	/**
	 * Returns a list of all of this graph's nodes' content.
	 */
	def getContents() {
		nodes.map[content].toList
	}
	
	/**
	 * Returns a list of all roots of this graph.
	 */
	def getRoots() {
		nodes.filter[isRoot].toList
	}
	
	/**
	 * Returns a list of all leaves of this graph.
	 */
	def getLeaves() {
		nodes.filter[isLeaf].toList
	}
	
	/**
	 * Returns a list of independent subgraphs of this graph.
	 */
	def getSubgraphs() {
		val queue = new LinkedList(nodes)
		val subgraphs = new LinkedList<DirectedGraph<T>>
		while (!queue.empty) {
			val node = queue.removeFirst
			val subgraph = node.subgraph
			subgraphs.add(subgraph)
			queue.removeIf[subgraph.contains(content)]
		}
		return subgraphs
	}
	
	/**
	 * Returns a list of all nodes contained by this graph, sorted
	 * topologically (roots to leaves).
	 * @throws GraphContainsCyclesException The elements cannot be sorted
	 *                                      topologically, because the graph
	 *                                      contains cycles.
	 */
	def getTopSortedNodes() throws GraphContainsCyclesException {
		if (nodes.empty) {
			return #[]
		}
		val sorted = new LinkedList<Node<T>>
		var queue = roots.toSet
		if (queue.empty) {
			throw new GraphContainsCyclesException
		}
		while (!queue.empty) {
			for (node: queue) {
				if (sorted.contains(node)) {
					throw new GraphContainsCyclesException
				}
				sorted.add(node)
			}
			queue = queue.flatMap[children].toSet
		}
		return sorted
	}
	
	/**
	 * Returns a list of the contents of all nodes contained by this graph,
	 * sorted topologically (roots to leaves).
	 * @throws GraphContainsCyclesException The elements cannot be sorted
	 *                                      topologically, because the graph
	 *                                      contains cycles.
	 */
	def getTopSortedContents() throws GraphContainsCyclesException {
		topSortedNodes.map[content]
	}
	
	/**
	 * Whether or not this graph contains any nodes.
	 */
	def isEmpty() {
		nodes.empty
	}
	
	/**
	 * Whether or not this graph's nodes are all connected to another, such
	 * that there are no independent subtrees.
	 */
	def isConnected() {
		subgraphs.size <= 1
	}
	
	/**
	 * Whether or not this graph contains cycles.
	 */
	def isCyclic() {
		try {
			topSortedNodes
		}
		catch (GraphContainsCyclesException e) {
			return true
		}
		return false
	}
	
	/**
	 * Whether or not this graph is a tree.
	 * <p>
	 * Trees are connected, non-cyclic graphs, that have exactly one root node.
	 * Furthermore there is only one distinct path from the root to any leaf.
	 */
	def isTree() {
		val roots = roots
		if (roots.size != 1 || !isConnected || isCyclic) {
			return false
		}
		return isTreeRecursive(roots.head, new HashSet)
	}
	
	private def boolean isTreeRecursive(Node<T> node, Set<Node<T>> visitedLeaves) {
		if (node.isLeaf) {
			if (visitedLeaves.contains(node)) {
				return false
			}
			else {
				visitedLeaves.add(node)
				return true
			}
		}
		return node.children.forall[ child | isTreeRecursive(child, visitedLeaves) ]
	}
	
	/**
	 * Whether or not this graph is a forest.
	 * <p>
	 * A forest's subgraphs are all trees.
	 * 
	 */
	def isForest() {
		subgraphs.forall[isTree]
	}
	
	override toString() {
		'''«class.simpleName» {«nodes.sortBy[content.toString].join("\n\t", ",\n\t", "\n") [toString]»}'''
	}
	
	/**
	 * Prints a reprensentation of this graph to the console and returns the
	 * graph itself. 
	 */
	def print() {
		println(toString)
		return this
	}
	
	
	
	/**
	 * The element class for {@link DirectedGraph}s.
	 */
	static class Node<T> {
		
		/**
		 * The content of this node.
		 */
		@Accessors(PUBLIC_GETTER)
		val T content
		
		/**
		 * The graph that contains this node.
		 */
		@Accessors(PUBLIC_GETTER)
		val DirectedGraph<T> graph
		
		/**
		 * The set of parent nodes of this node.
		 */
		val Set<Node<T>> parents
		
		/**
		 * The set of child nodes of this node.
		 */
		val Set<Node<T>> children
		
		private new (T content, DirectedGraph<T> graph) {
			this.content = content
			this.graph = graph
			this.parents = new HashSet
			this.children = new HashSet
		}
		
		/**
		 * This is a convenience method for {@link DirectedGraph#getNode(T)
		 * DirectedGraph.getNode(T)}.
		 * <p>
		 * Returns an existing node of this node's graph, that contains
		 * {@code content}. If there is no such node, {@code null} is returned.
		 */
		def getNode(T content) {
			graph.getNode(content)
		}
		
		/**
		 * This is a convenience method for {@link DirectedGraph#addNode(T)
		 * DirectedGraph.addNode(T)}.
		 * <p>
		 * Returns an existing node of this node's graph, that contains
		 * {@code content}. If there is no such node, a new node will be
		 * created and added to the graph.
		 */
		def addNode(T content) {
			graph.addNode(content)
		}
		
		/**
		 * Returns a list of all of this node's parents.
		 */
		def getParents() {
			parents.toList
		}
		
		/**
		 * Adds parents to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addParents(Iterable<T> parents) {
			for (parent: parents) {
				val parentNode = graph.addNode(parent)
				this.parents.add(parentNode)
				parentNode.children.add(this)
			}
			return this
		}
		
		/**
		 * Adds parents to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addParents(T ... parents) {
			addParents(parents.toList)
		}
		
		/**
		 * Returns a list of all of this node's children.
		 */
		def getChildren() {
			children.toList
		}
		
		/**
		 * Adds children to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addChildren(Iterable<T> children) {
			for (child: children) {
				val childNode = graph.addNode(child)
				this.children.add(childNode)
				childNode.parents.add(this)
			}
			return this
		}
		
		/**
		 * Adds children to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addChildren(T ... children) {
			addChildren(children.toList)
		}
		
		/**
		 * Returns a list of all of this node's ancestors.
		 */
		def getAncestors() {
			val ancestorNodes = new HashSet<Node<T>>
			getAncestorsRecursive(ancestorNodes)
			return ancestorNodes.toList
		}
		
		private def void getAncestorsRecursive(Set<Node<T>> ancestorNodes) {
			for (parent: parents) {
				if (ancestorNodes.add(parent)) {
					parent.getAncestorsRecursive(ancestorNodes)
				}
			}
		}
		
		/**
		 * Returns a list of all of this node's descendants.
		 */
		def getDescendants() {
			val descendantNodes = new HashSet<Node<T>>
			getDescendantsRecursive(descendantNodes)
			return descendantNodes.toList
		}
		
		private def void getDescendantsRecursive(Set<Node<T>> descendantNodes) {
			for (child: children) {
				if (descendantNodes.add(child)) {
					child.getDescendantsRecursive(descendantNodes)
				}
			}
		}
		
		/**
		 * Returns a subgraph of this node's graph, that contains this node.
		 */
		def getSubgraph() {
			val nodes = new HashSet<Node<T>>
			getSubgraphRecursive(nodes)
			val subgraph = new DirectedGraph<T>
			for (node: nodes) {
				subgraph
					.addNode(node.content)
					.addParents(node.parents.map[content])
					.addChildren(node.children.map[content])
			}
			return subgraph
		}
		
		private def void getSubgraphRecursive(Set<Node<T>> nodes) {
			if (nodes.add(this)) {
				for (parent: parents) {
					parent.getSubgraphRecursive(nodes)
				}
				for (child: children) {
					child.getSubgraphRecursive(nodes)
				}
			}
		}
		
		/**
		 * Whether this node is a root node.
		 */
		def isRoot() {
			parents.empty
		}
		
		/**
		 * Whether this node is a leaf node.
		 */
		def isLeaf() {
			children.empty
		}
		
		override equals(Object other) {
			switch other {
				Node<T>: this.content.equals(other.content)
				default: false
			}
		}
		
		override toString() {
			'''«content.toString» -> {«children.sortBy[content.toString].join(", ") [content.toString]»}'''
		}
		
		/**
		 * Prints a reprensentation of this node to the console and returns the
		 * node itself. 
		 */
		def print() {
			println(toString)
			return this
		}
		
	}
	
	
	
	/**
	 * Indicates that a method has encountered an error, because the graph
	 * contains one or more cycles.
	 */
	static class GraphContainsCyclesException extends RuntimeException {
		
		new () {
			super()
		}
		
		new (String message) {
			super(message)
		}
		
	}
	
}
