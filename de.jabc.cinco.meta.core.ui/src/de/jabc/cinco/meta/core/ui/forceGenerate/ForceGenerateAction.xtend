package de.jabc.cinco.meta.core.ui.forceGenerate

import de.jabc.cinco.meta.core.ui.handlers.CincoProductGenerationHandler
import org.eclipse.jface.action.IAction
import org.eclipse.jface.viewers.ISelection
import org.eclipse.ui.IActionDelegate
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.handlers.IHandlerService

class ForceGenerateAction implements IActionDelegate {
	
	new() {
		// Intentionally left blank
	}

	override void run(IAction action) {
		CincoProductGenerationHandler.forceNextGeneration();
		val handlerService = PlatformUI.getWorkbench().getService(IHandlerService)
		handlerService.executeCommand("de.jabc.cinco.meta.core.ui.cincoproductgenerationcommand", null)
	}
	
	override selectionChanged(IAction action, ISelection selection) {
		// Intentionally left blank
	}
	
}
