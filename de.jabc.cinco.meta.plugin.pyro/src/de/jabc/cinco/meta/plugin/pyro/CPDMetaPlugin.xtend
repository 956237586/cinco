package de.jabc.cinco.meta.plugin.pyro

import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin
import java.io.IOException
import java.net.URISyntaxException
import java.util.List
import mgl.MGLModel
import org.eclipse.core.resources.IProject
import productDefinition.Annotation
import productDefinition.CincoProduct

class CPDMetaPlugin implements ICPDMetaPlugin {
	
	new() {
		println("[Pyro] Awaiting your command")
	}
	
	override executeCPDMetaPlugin(List<Annotation> cpdAnnotations,
	                              List<MGLModel> generatedMGLs,
	                              List<MGLModel> allMGLs,
	                              CincoProduct cpd,
	                              IProject mainProject) {
		var CreatePyroPlugin cpp = new CreatePyroPlugin()
		val pyroAnnotation = cpd.annotations.findFirst[name.equals("pyro")]
		if (pyroAnnotation === null) {
			throw new IllegalStateException("[Pyro] pyro annotation is not provided")
		}
		val values = pyroAnnotation.value
		if (values.nullOrEmpty) {
			throw new IllegalStateException("[Pyro] pyro target path is missing")
		}
		try {
			println("[Pyro] Aye Sir, starting generation!")
			cpp.execute(generatedMGLs.toSet, mainProject, cpd, values.head)
			println("[Pyro] Sir. Generation completet successfully, Sir!")
		}
		catch (IOException | URISyntaxException e) {
			e.printStackTrace()
		}
	}
	
}
