package de.jabc.cinco.meta.plugin.pyro.util

import java.util.Set
import mgl.GraphModel
import mgl.MGLModel

class GeneratorCompound {
	public final String projectName;
	public final Set<MGLModel> mglModels
	
	new(String projectName,Set<MGLModel> mglModels) {
		this.projectName = projectName
		this.mglModels = mglModels
	}
}