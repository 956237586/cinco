package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class GenmodelTmpl extends EcoreTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	override getTargetFileName() '''«model.fileName»Gratext.genmodel'''
	
	def targetName() '''«model.fileName»Gratext'''
	
	override template() '''	
		<?xml version="1.0" encoding="UTF-8"?>
		<genmodel:GenModel xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" xmlns:genmodel="http://www.eclipse.org/emf/2002/GenModel" runtimeVersion="2.10"
		    modelName="«targetName»" complianceLevel="8.0" copyrightFields="false"
		modelDirectory="/«project.symbolicName»/model-gen"
		modelPluginID="«project.symbolicName»"
		editPluginID="«project.symbolicName».edit"
		editorPluginID="«project.symbolicName».editor"
		testsPluginID="«project.symbolicName».tests">
			«val ecoreFileName = EcoreTmpl.getTargetFileName»
			<genPackages prefix="«targetName»" basePackage="«model.package».«graphModel.name.toLowerCase»" disposableProviderFactory="true" ecorePackage="«ecoreFileName»#/">
				<genClasses ecoreClass="«ecoreFileName»#//«model.fileName»"/>
				«FOR node:MGLUtil.nodes(graphModel)»
					<genClasses ecoreClass="«ecoreFileName»#//«node.name»">
					</genClasses>
				«ENDFOR»
				«FOR gmEntry : MGLUtil.edgesWithOrigins(graphModel).entrySet»
					«FOR edge : gmEntry.value»
						<genClasses ecoreClass="«ecoreFileName»#//«edge.name»"/>
					«ENDFOR»
				«ENDFOR»
				«FOR cls:classes»
					«cls.toGenmodelXMI(ecoreFileName)»
				«ENDFOR»
			</genPackages>
			<usedGenPackages href="platform:/resource/«model.projectSymbolicName»/src-gen/model/«cpd.name».genmodel#//«cpd.name.toLowerCase»"/>
			«FOR externalModelImport : model.imports.filter[isExternal]»
				<usedGenPackages href="«externalModelImport.importURI.split("/model/").get(0)»/src-gen/model/«"ExternalGraphTool"».genmodel#//«"externalgraphtool"»"/>
			«ENDFOR»
			<usedGenPackages href="platform:/plugin/de.jabc.cinco.meta.core.mgl.model/model/GraphModel.genmodel#//graphmodel"/>
		</genmodel:GenModel>
	'''
	
}