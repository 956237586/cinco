package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class FormatterTmpl extends FileTemplate {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	override getTargetFileName() '''«model.fileName»GratextFormatter.xtend'''
	
	override template() '''	
		package «package»
		
		import com.google.inject.Inject
		import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
		import org.eclipse.xtext.formatting.impl.FormattingConfig
		import «basePackage».services.*
		
		/**
		 * This class contains custom formatting declarations.
		 */
		public class «model.fileName»GratextFormatter extends AbstractDeclarativeFormatter {
			
			@Inject extension «model.fileName»GratextGrammarAccess
			
			override protected configureFormatting(FormattingConfig c) {
				for (pair : findKeywordPairs('{', '}')) {
					c.setIndentation(pair.first, pair.second)
					c.setLinewrap(1).after(pair.first)
					c.setLinewrap(1).before(pair.second)
					c.setLinewrap(1).after(pair.second)
				}
				for (comma : findKeywords(',')) {
					c.setNoLinewrap().before(comma)
					c.setNoSpace().before(comma)
					c.setLinewrap().after(comma)
				}
				c.setLinewrap(0, 1, 2).before(get_SL_COMMENTRule)
				c.setLinewrap(0, 1, 2).before(get_ML_COMMENTRule)
				c.setLinewrap(0, 1, 1).after(get_ML_COMMENTRule)
			}
		}
	'''
	
}