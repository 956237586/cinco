package de.jabc.cinco.meta.plugin.gratext;

import java.util.List;

import org.eclipse.core.resources.IProject;

import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin;
import mgl.MGLModel;
import productDefinition.Annotation;
import productDefinition.CincoProduct;

public class CPDMetaPlugin implements ICPDMetaPlugin {

	public CPDMetaPlugin() {	
		// Intentionally left blank
	}

	@Override
	public void executeCPDMetaPlugin(List<Annotation> cpdAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject) {
		// Intentionally left blank
	}

}
