package de.jabc.cinco.meta.plugin.gratext;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;

import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;
import de.jabc.cinco.meta.core.utils.MGLUtil;
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import de.jabc.cinco.meta.runtime.xapi.FileExtension;
import mgl.GraphModel;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class GratextGenerationHandler extends AbstractHandler {

	private static FileExtension fileHelper = new FileExtension();
	
	@SuppressWarnings("restriction")
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IFile mglFile = MGLSelectionListener.INSTANCE.getCurrentMGLFile();
		if (mglFile == null) 
			return null;
		MGLModel model = fileHelper.getContent(mglFile, MGLModel.class, 0);
		
		for (MGLModel mgl: GeneratorUtils.getInstance().allMGLs) {
			if (MGLUtil.equalMGLModels(model, mgl)) {
				model = mgl;
			}
		}
		
		CincoProduct cpd = MGLSelectionListener.INSTANCE.getSelectedCPD();
		for (GraphModel graphModel: model.getGraphModels()) {
			if (!graphModel.isIsAbstract()) {
				new GratextPlugin().createGratextProjects(cpd, model, graphModel);
			}
		}
		
		return null;
		
	}
	
}
