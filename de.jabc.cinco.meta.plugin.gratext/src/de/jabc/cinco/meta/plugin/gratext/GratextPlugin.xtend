package de.jabc.cinco.meta.plugin.gratext

import de.jabc.cinco.meta.plugin.CincoMetaPlugin
import de.jabc.cinco.meta.plugin.gratext.tmpl.project.GratextProjectTmpl
import de.jabc.cinco.meta.plugin.gratext.tmpl.project.GratextUiProjectTmpl
import mgl.GraphModel
import mgl.MGLModel
import productDefinition.CincoProduct

class GratextPlugin extends CincoMetaPlugin {
	
	override executeCincoMetaPlugin() {
		for (mgl: generatedMGLs) {
			for (gm: mgl.graphModels) {
				createGratextProjects(cpd, mgl, gm)
			}
		}
	}
	
	def createGratextProjects(CincoProduct cpd, MGLModel mgl, GraphModel gm) {
		new GratextProjectTmpl => [
			it.cpd = cpd
			model = mgl
			graphModel = gm
			create
		]
		new GratextUiProjectTmpl => [
			it.cpd = cpd
			model = mgl
			graphModel = gm
			create
		]
	}
	
}
