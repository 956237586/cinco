package de.jabc.cinco.meta.core.ge.style.fragment;

import java.util.List;

import org.eclipse.core.resources.IProject;

import de.jabc.cinco.meta.core.pluginregistry.IMGLMetaPlugin;
import mgl.Annotation;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class GraphitiGenerator implements IMGLMetaPlugin {

	@Override
	public void executeMGLMetaPlugin(List<Annotation> mglAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject) {
		// Intentionally left blank
	}
	
}
