package de.jabc.cinco.meta.plugin.placeholder

import de.jabc.cinco.meta.plugin.placeholder.template.project.PlaceholderProjectTemplate
import de.jabc.cinco.meta.plugin.CincoMetaPlugin

class PlaceholderPlugin extends CincoMetaPlugin{
	
	override executeCincoMetaPlugin() {
		for (mgl: generatedMGLs) {
			for (gm: mgl.graphModels) {
				new PlaceholderProjectTemplate => [
					model = mgl
					graphModel = gm
					create
				]
			}
		}
	}
	
}
