package de.jabc.cinco.meta.plugin.ocl.templates

class ValidateActionTemplate {
	def create(String packageName)'''
		package «packageName»;
		
		import «packageName».ServiceLoader;
		
		import org.eclipse.jface.action.IAction;
		import org.eclipse.jface.viewers.ISelection;
		import org.eclipse.jface.viewers.IStructuredSelection;
		import org.eclipse.ui.IObjectActionDelegate;
		import org.eclipse.ui.IWorkbenchPart;
		
		public class ValidateAction implements IObjectActionDelegate {
		
			private IWorkbenchPart targetPart;
			
			/**
			 * Constructor for Action1.
			 */
			public ValidateAction() {
				super();
			}
		
			/**
			 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
			 */
			public void setActivePart(IAction action, IWorkbenchPart targetPart) {
				this.targetPart = targetPart; 
			}
		
			/**
			 * @see IActionDelegate#run(IAction)
			 */
			public void run(IAction action) {
				
				ISelection selection = targetPart.getSite().getPage().getSelection();
				
				if(selection instanceof IStructuredSelection){
					IStructuredSelection isl = (IStructuredSelection) selection;
					ServiceLoader.oclValidation(isl,false);
				}
						
			}
			
			public void run(IAction action,boolean isOnSave) {
				
				ISelection selection = targetPart.getSite().getPage().getSelection();
				
				if(selection instanceof IStructuredSelection){
					IStructuredSelection isl = (IStructuredSelection) selection;
					ServiceLoader.oclValidation(isl,isOnSave);
				}
						
			}
			
			/**
			 * @see IActionDelegate#selectionChanged(IAction, ISelection)
			 */
			public void selectionChanged(IAction action, ISelection selection) {
			}
		
		}

	'''
}