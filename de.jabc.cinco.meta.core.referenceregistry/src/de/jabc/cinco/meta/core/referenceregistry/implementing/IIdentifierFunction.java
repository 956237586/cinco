package de.jabc.cinco.meta.core.referenceregistry.implementing;

import org.eclipse.emf.ecore.EObject;

public interface IIdentifierFunction {

	public String getIdentifier(EObject eobj);
	
}
