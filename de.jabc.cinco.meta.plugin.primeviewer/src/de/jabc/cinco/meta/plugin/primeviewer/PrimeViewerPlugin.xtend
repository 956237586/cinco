package de.jabc.cinco.meta.plugin.primeviewer

import de.jabc.cinco.meta.plugin.CincoMetaPlugin
import de.jabc.cinco.meta.plugin.primeviewer.tmpl.project.PrimeViewerProjectTmpl

class PrimeViewerPlugin extends CincoMetaPlugin {
	
	override executeCincoMetaPlugin() {
		for (mgl: allMGLs) {
			for (gm: mgl.graphModels) {
				new PrimeViewerProjectTmpl => [
					model = mgl
					graphModel = gm
					create
				]
			}
		}
	}
	
}
