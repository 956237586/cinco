package de.jabc.cinco.meta.core.ge.style.generator.runtime.api;

import java.util.ArrayList;

import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.ChopboxAnchor;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.PictogramsFactory;
import org.eclipse.graphiti.mm.pictograms.Shape;

import graphmodel.Node;

public interface CNode extends CModelElement, Node{
	
	public default Anchor getAnchor() {
		PictogramElement pe = getPictogramElement();
		if (pe instanceof AnchorContainer) {
			AnchorContainer ac = ((AnchorContainer) pe);
			if (((AnchorContainer) pe).getAnchors().isEmpty()) {
				if(pe instanceof ContainerShape) {
					ArrayList<PictogramElement> childrenToCheckForAnchors = new ArrayList<PictogramElement>();
					childrenToCheckForAnchors.addAll(((ContainerShape) pe).getChildren());
					for(int i = 0; i < childrenToCheckForAnchors.size(); i++) {
						PictogramElement peChild = childrenToCheckForAnchors.get(i);
						if (!((AnchorContainer) peChild).getAnchors().isEmpty()) {
							return ((AnchorContainer) peChild).getAnchors().get(0);
						} else {
							if(peChild instanceof ContainerShape) {
								for(Shape child : ((ContainerShape) peChild).getChildren()) {
									childrenToCheckForAnchors.add(child);
								}
							}
						}
					}
				}
				
				ChopboxAnchor anchor = PictogramsFactory.eINSTANCE.createChopboxAnchor();
				anchor.setActive(false);
				anchor.setParent(ac);
			}
			return ((AnchorContainer) pe).getAnchors().get(0);
		} else throw new RuntimeException(String.format("Could not retrieve anchors from PictogramElement %s", pe));
	};
}
