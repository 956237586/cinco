package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.graphiti.internal.IDiagramVersion
import org.eclipse.graphiti.mm.algorithms.styles.StylesFactory
import org.eclipse.graphiti.mm.algorithms.styles.StylesPackage
import org.eclipse.graphiti.mm.pictograms.PictogramsPackage
import org.eclipse.graphiti.mm.pictograms.impl.DiagramImpl
import org.eclipse.graphiti.ui.editor.DiagramBehavior
import org.eclipse.graphiti.util.IColorConstant

import static org.eclipse.graphiti.internal.util.LookManager.getLook
import static org.eclipse.graphiti.services.Graphiti.getGaService

abstract class LazyDiagram extends DiagramImpl {
	
	static extension val WorkbenchExtension = new WorkbenchExtension
	
	String dtpId
	Runnable initialization
	boolean initialized
	boolean initializing
	boolean avoidInitialization
	DiagramBehavior diagramBehavior
	
	new(String name, String dtpId) {
		super()
		setDiagramTypeId(name)
		setDiagramTypeProviderId(dtpId)
		setGridUnit(look.getMinorGridLineDistance)
		setSnapToGrid(true)
		setVisible(true)
		setName(name)
		eSet(PictogramsPackage.eINSTANCE.getDiagram_Version, IDiagramVersion.CURRENT)
		val rect = gaService.createRectangle(this) => [
			foreground = look.getMinorGridLineColor.addColor
			background = look.getGridBackgroundColor.addColor
		]
		gaService.setSize(rect, 1000, 1000)
	}
	
	def initialize() {
		if (initialization !== null) {
			initialization.run
		}
	}
	
	def assertInitialized() {
		if (!initialized && !avoidInitialization && !initializing ) {
			val time = System.currentTimeMillis
			initializing = true
			transact[ initialize ]
			println('''[«this.class.simpleName»-«hashCode»] initialized in «(System.currentTimeMillis - time)» ms''')
			initializing = false
			initialized = true
		}
	}
	
	def setInitialization(Runnable rbl) {
		initialization = rbl
	}
	
	override getAnchors() {
		assertInitialized
		super.getAnchors
	}
	
	override getChildren() {
		assertInitialized
		super.getChildren
	}
	
	override getColors() {
		assertInitialized
		super.getColors
	}
	
	override getConnections() {
		assertInitialized
		super.getConnections
	}
	
	override getDiagramTypeId() {
		assertInitialized
		super.getDiagramTypeId
	}
	
	def String getDiagramTypeProviderId() {
		this.dtpId
	}
	
	override getFonts() {
		assertInitialized
		super.getFonts
	}
	
	override getGraphicsAlgorithm() {
		assertInitialized
		super.getGraphicsAlgorithm
	}
	
	override getName() {
		assertInitialized
		super.getName
	}
	
	override getPictogramLinks() {
		assertInitialized
		super.getPictogramLinks
	}
	
	override getStyles() {
		assertInitialized
		super.getStyles
	}
	
	def setDiagramTypeProviderId(String dtpId) {
		this.dtpId = dtpId
	}
	
	def setAvoidInitialization(boolean flag) {
		avoidInitialization = flag
	}
	
	private def addColor(IColorConstant colconst) {
		val pkg = StylesPackage.eINSTANCE
		val col = StylesFactory.eINSTANCE.createColor => [
			eSet(pkg.color_Red, colconst.red)
			eSet(pkg.color_Green, colconst.green)
			eSet(pkg.color_Blue, colconst.blue)
		]
		super.getColors.add(col)
		return col
	}
	
	def getDiagramBehavior() {
		diagramBehavior
	}
	
	def setDiagramBehavior(DiagramBehavior db) {
		diagramBehavior = db
	}
}
