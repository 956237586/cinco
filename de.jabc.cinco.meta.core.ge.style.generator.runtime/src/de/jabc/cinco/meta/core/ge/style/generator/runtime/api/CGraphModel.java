package de.jabc.cinco.meta.core.ge.style.generator.runtime.api;

import org.eclipse.graphiti.features.IFeatureProvider;

public interface CGraphModel extends CModelElement{

	public IFeatureProvider getFeatureProvider();
	public void setFeatureProvider(IFeatureProvider fp);
	
}
