package de.jabc.cinco.meta.core.ge.style.generator.runtime.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.impl.DefaultReconnectionFeature;

public class CincoReconnectFeature extends DefaultReconnectionFeature{

	public CincoReconnectFeature(IFeatureProvider fp) {
		super(fp);
	}
}
