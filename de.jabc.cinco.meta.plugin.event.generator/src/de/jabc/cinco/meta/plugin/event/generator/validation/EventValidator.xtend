package de.jabc.cinco.meta.plugin.event.generator.validation

import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator
import de.jabc.cinco.meta.plugin.event.generator.util.ClassLoaderExtension
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import java.util.regex.Pattern
import mgl.Annotation
import mgl.Edge
import mgl.GraphModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jdt.core.IType
import org.eclipse.jdt.core.JavaCore

import static de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult.*

class EventValidator implements IMetaPluginValidator {
	
	extension EventGeneratorExtension = new EventGeneratorExtension
	extension ClassLoaderExtension = new ClassLoaderExtension
	
	val static FQN_PATTERN = Pattern.compile('''^([a-zA-Z_$][a-zA-Z0-9_$]*\.)*[a-zA-Z_$][a-zA-Z0-9_$]*$''')
	
	override checkAll(EObject it) {
		switch it {
			Annotation case name == eventAnnotationName: checkEventAnnotation
		}
	}
	
	def private checkEventAnnotation(Annotation it) {
		switch parent {
			GraphModel:    checkModelElementEventAnnotation(true)
			NodeContainer,
			Node,
			Edge:          checkModelElementEventAnnotation(false)
			default:       newError('''Event annotation incompatible with: «parent.class.simpleName»''', nameFeature)
		}
	}

	def private checkModelElementEventAnnotation(Annotation it, boolean allowZeroArguments) {
		// Check number of arguments
		if (allowZeroArguments && value.nullOrEmpty) {
			return null // No error
		}
		if (value === null || value.size != 1) {
			return newError('Provide exactly one FQN.', nameFeature)
		}
		// Check FQN pattern
		val fqn = value.head
		if (!FQN_PATTERN.matcher(fqn).matches) {
			return newError('''"«fqn»" is not a valid Java FQN.''', valueFeature)
		}
		// Check for duplicate event FQNs
		val parent = parent as ModelElement
		val model = parent.model
		val allElements = model.graphModels + model.nodes + model.edges
		val allEventAnnotations = allElements.map[annotations].flatten.filter[isEventAnnotation]
		if (allEventAnnotations.exists[ anno | fqn == anno.value.head && it !== anno ]) {
			return newError('''"«fqn»" is already used by another element.''', valueFeature)
		}
		// Check if FQN matches a generated class from src-gen
		val lowerCaseFqn = fqn.toLowerCase
		if (allElements.exists[ element | lowerCaseFqn == element.eventGeneratedFqn.fullyQualifiedName.toLowerCase ]) {
			return newError('''"«fqn»" cannot be used, because it is used internally.''', valueFeature)
		}
		// Check if class exists
		val type = fqn.findClass
		if (type === null || !type.exists) {
			return newInfo('''Java class "«fqn»" does not exist. A template will be generated.''', valueFeature)
		}
		// Check if package exists
		if (findPackage(fqn) === null) {
			return newError('''Package of "«fqn»" not found.''', valueFeature)
		}
		// Check if Java class implements correct event interface
		try {
			refreshClassLoaders
			val classFromAnnotation = fqn.loadClass
			val generatedClass = parent.eventGeneratedFqn.fullyQualifiedName.loadClass
			if (!classFromAnnotation.implementsOrExtends(generatedClass)) {
				return newWarning('''"«fqn»" must extend the class "«generatedClass»".''', valueFeature)
			}
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace
		}
	}
	
	def private findClass(String fqn) {
		var IType javaClass = null
		val root = ResourcesPlugin.workspace.root
		val projects = root.projects
		for (project: projects) {
			var jproject = JavaCore.create(project) as IJavaProject
			if (jproject.exists) {
				try {
					javaClass = jproject.findType(fqn)
					if (javaClass !== null) {
						return javaClass 
					}
				}
				catch (Exception e) {
					// Nothing to do here (?)
				}
			}
		}
		return javaClass
	}
	
	def private findPackage(String fqn) { 
		var IType javaClass = null
		val root = ResourcesPlugin.workspace.root
		val projects = root.projects
		for (project: projects) {
			var jproject = JavaCore.create(project) as IJavaProject
			if (jproject.exists) {
				try {
					javaClass = jproject.findType(fqn)
					if (javaClass !== null) {
						return jproject
					}
				}
				catch (Exception e) {
					// Nothing to do here (?)
				}
			}
		}
		return null
	}
	
	def private getNameFeature(Annotation it) {
		eClass.getEStructuralFeature('name')
	}
	
	def private getValueFeature(Annotation it) {
		eClass.getEStructuralFeature('value')
	}
		
}
