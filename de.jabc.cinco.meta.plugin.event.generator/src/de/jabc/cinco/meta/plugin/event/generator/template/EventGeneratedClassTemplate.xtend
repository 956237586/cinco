package de.jabc.cinco.meta.plugin.event.generator.template

import de.jabc.cinco.meta.core.event.hub.Subscriber
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import de.jabc.cinco.meta.plugin.event.generator.util.XtendFileTemplate
import java.util.List
import mgl.ModelElement

import static de.jabc.cinco.meta.plugin.event.api.util.EventEnum.POST_DELETE

class EventGeneratedClassTemplate extends XtendFileTemplate {
	
	extension EventGeneratorExtension = new EventGeneratorExtension
	
	val ModelElement element
	val ModelElement superElement
	
	new (ModelElement element) {
		this.element = element
		this.superElement = element.superElement
	}
	
	override getClassName() {
		element.eventGeneratedClassName
	}
	
	override getClassTemplate() '''
		«IF element.hasEventAnnotation»abstract«ELSE»final«ENDIF» class «className» implements «element.eventApiFqn.fqn»<«element.fqn»> {
			
			protected extension «EventApiExtension.fqn» = new «EventApiExtension.fqn»
			
			var static «className» eventInstance
			
			var «List.fqn»<«Subscriber.fqn»> subscribers
			
			protected new () {
				// Intentionally left blank
			}
			
			def final static «className» getInstance() {
				if (eventInstance === null) {
					eventInstance = new «if (element.hasEventAnnotation) element.eventUserFqn.fqn else element.eventGeneratedFqn.fqn»
				}
				return eventInstance
			}
			
			override final subscribe() {
				subscribers = #[
					«FOR event: element.events SEPARATOR ','»
						subscribePayloadSubscriber('«event.getContextIdentifier(element)»') [
							«event.payloadClass.fqn»<«element.fqn»> payload |
								instance.«event.methodName»(payload)
						]
					«ENDFOR»
				]
			}
			
			override final unsubscribe() {
				subscribers?.forEach [ unsubscribe ]
				subscribers = null
			}
			«FOR event: element.events»
				
				override«IF !event.isImplemented» final«ENDIF» «event.methodName»(«event.getMethodParameterDeclarations(element.elementFqn) [ fqn() ]») {
					«IF superElement === null || !superElement.isEventEnabled»
						«IF event === POST_DELETE»
							[ /* Intentionally left blank */ ]
						«ELSE»
							// Intentionally left blank
						«ENDIF»
					«ELSE»
						«superElement.eventGeneratedFqn.fqn».instance.«event.methodName»(«event.methodParameterNames»)
					«ENDIF»
				}
				
				override final «event.methodName»(«event.payloadClass.fqn»<«element.fqn»> it) {
					«event.methodName»(«event.methodParameterNames»)
				}
			«ENDFOR»
			
		}
	'''
	
}