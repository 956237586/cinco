package de.jabc.cinco.meta.plugin.event.generator.template

import de.jabc.cinco.meta.plugin.event.generator.util.XtendFileTemplate
import de.jabc.cinco.meta.runtime.IStartup
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import mgl.MGLModel

class EventStartupClassTemplate extends XtendFileTemplate {
	
	extension EventGeneratorExtension = new EventGeneratorExtension
	
	val MGLModel mgl
	
	new (MGLModel mgl) {
		this.mgl = mgl
	}
	
	override getClassName() {
		'EventStartup'
	}
	
	override getClassTemplate() '''
		class «className» implements «IStartup.fqn» {
			
			override startup() {
				subscribe
			}
			
			«val elements = mgl.eventEnabledElements»
			def static void subscribe() {
				«FOR element: elements»
					«element.eventGeneratedFqn.fqn».instance.subscribe
				«ENDFOR»
			}
			
			def static void unsubscribe() {
				«FOR element: elements»
					«element.eventGeneratedFqn.fqn».instance.unsubscribe
				«ENDFOR»
			}
			
		}
	'''
	
}
