package de.jabc.cinco.meta.plugin.event.generator

import de.jabc.cinco.meta.plugin.event.generator.template.MainProjectTemplate
import de.jabc.cinco.meta.plugin.CincoMetaPlugin

class EventPlugin extends CincoMetaPlugin {
	
	val public static String PLUGIN_ID = 'de.jabc.cinco.meta.plugin.event.generator'
	
	override executeCincoMetaPlugin() {
		MainProjectTemplate.create
	}
	
}
