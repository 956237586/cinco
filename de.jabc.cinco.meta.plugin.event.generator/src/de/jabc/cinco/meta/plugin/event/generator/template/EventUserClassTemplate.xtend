package de.jabc.cinco.meta.plugin.event.generator.template

import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import de.jabc.cinco.meta.plugin.event.generator.util.XtendFileTemplate
import mgl.ModelElement

import static de.jabc.cinco.meta.plugin.event.api.util.EventEnum.POST_DELETE

class EventUserClassTemplate extends XtendFileTemplate {
	
	extension EventGeneratorExtension = new EventGeneratorExtension
	
	var boolean generateExampleLogs = false
	
	val ModelElement element
	val ModelElement superElement
	val boolean hasSuperElement
	val Iterable<EventEnum> events
	
	new (ModelElement element) {
		this.element         = element
		this.superElement    = element.superElement
		this.hasSuperElement = superElement !== null
		this.events          = element.events.filter[ isImplemented ]
	}
	
	override getClassName() {
		element.eventUserClassName
	}
	
	override getClassTemplate() '''
		/* 
		 * About this class:
		 * - This is a default implementation for «element.eventGeneratedFqn.fqn».
		 * - This class was generated, because you added an "@event" annotation to
		 *   «element.graphmodelClass.simpleName» "«element.elementClassName»" in "«element.model.modelFileName».mgl".
		 * - This file will not be overwritten on future generation processes.
		«IF hasSuperElement»
			«null /* Fixes indentation */»
			 * 
			 * Edit this class:
			 * - If you wish «element.elementClassName» to react the same way as its super class «superElement.elementClassName»,
			 *   you may delete the method or leave it as is (with only the super call).
			 * - If you wish to only add functionality, leave the super call in the
			 *   corresponding method and add your code to it.
			 * - If you wish to break the inheritance chain, remove the super call, but do
			 *   not delete the corresponding method. You may leave it empty or write new
			 *   code.
		«ENDIF»
		 * 
		 * Available event methods:
		«FOR event: events»
			«null /* Fixes indentation */»
			 * - «event.methodName»(«event.getMethodParameterDeclarations(element.elementFqn) [ fqn() ]»)
		«ENDFOR»
		 */
		final class «className» extends «element.eventGeneratedFqn.fqn» {
			«FOR event: events»
				
				override «event.methodName»(«event.getMethodParameterDeclarations(element.elementFqn) [ fqn() ]») {
					// TODO: Auto-generated method stub
					«IF event === POST_DELETE»
						// Set up your post delete Runnable here.
						// This will be executed pre delete.
						«IF hasSuperElement»
							val super«event.methodName.toFirstUpper» = super.«event.methodName»(«event.methodParameterNames»)
						«ENDIF»
						return [
							// This is your post delete Runnable.
							// This will be executed post delete.
							«IF hasSuperElement»
								super«event.methodName.toFirstUpper».run
							«ENDIF»
							«event.logCall»
						]
					«ELSE»
						«IF hasSuperElement»
							super.«event.methodName»(«event.methodParameterNames»)
						«ENDIF»
						«event.logCall»
					«ENDIF»
				}
			«ENDFOR»
			
		}
	'''
	
	def private getLogCall(EventEnum event) {
		if (generateExampleLogs) {
			'''
				log(«"'''"»
					«className».«event.methodName»(
						«FOR parameter: event.payloadClass.declaredFields.map[name] SEPARATOR ','»
							«parameter» = «'«'»«parameter»«'»'»
						«ENDFOR»
					)
				«"'''"»)
			'''
		}
	}
	
}