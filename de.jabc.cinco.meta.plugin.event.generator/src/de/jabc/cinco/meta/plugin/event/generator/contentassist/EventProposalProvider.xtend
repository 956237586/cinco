package de.jabc.cinco.meta.plugin.event.generator.contentassist

import de.jabc.cinco.meta.core.pluginregistry.proposalprovider.IMetaPluginAcceptor
import mgl.Annotation

class EventProposalProvider implements IMetaPluginAcceptor {
	
	override getAcceptedStrings(Annotation annotation) {
		null
	}
	
	override getTextApplier(Annotation annotation) {
		null
	}
	
}
