package de.jabc.cinco.meta.plugin.executer.service;

import java.util.List;

import org.eclipse.core.resources.IProject;

import de.jabc.cinco.meta.core.pluginregistry.IMGLMetaPlugin;
import de.jabc.cinco.meta.plugin.executer.generator.model.CreateExecuterPlugin;
import mgl.Annotation;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class ESDSLMetaPluginService implements IMGLMetaPlugin {

	@Override
	public void executeMGLMetaPlugin(List<Annotation> mglAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject) {
		System.out.println("RUNNING EXECUTION SEMANTICS METAPLUGIN");
		for (var mgl: allMGLs) {
			for (var graphModel: mgl.getGraphModels()) {
				try {
					new CreateExecuterPlugin().execute(graphModel);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
