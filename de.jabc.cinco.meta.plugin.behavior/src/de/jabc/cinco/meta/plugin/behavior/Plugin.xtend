package de.jabc.cinco.meta.plugin.behavior

import de.jabc.cinco.meta.plugin.behavior.templates.project.ProjectTemplate
import de.jabc.cinco.meta.plugin.CincoMetaPlugin

class Plugin extends CincoMetaPlugin {
	
	override executeCincoMetaPlugin() {
		val template = ProjectTemplate.newInstanceWithContext(this)
		for (mgl: generatedMGLs) {
			template.model = mgl
			for (gm: mgl.graphModels) {
				template.graphModel = gm
				template.create
			}
		}
	}
	
}
