package de.jabc.cinco.meta.runtime.hook

import org.eclipse.emf.ecore.EObject
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.Direction

abstract class CincoPostResizeHook<T extends EObject> extends CincoRuntimeBaseClass {
	
	/**
	 * This method is called after resizing a graphical element on the canvas.
	 * @param modelElement The modelElement which is linked to the resized graphical representation.
	 * @param oldWidth The width before the resize operation.
	 * @param oldHeight The height before the resize operation.
	 * @param oldX The x-coordinate before the resize operation.
	 * @param oldY The y-coordinate before the resize operation.
	 * @param direction The direction of the resize operation.
	 */
	def void postResize(T modelElement, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction)
	
}
