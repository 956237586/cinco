package de.jabc.cinco.meta.runtime.hook

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.GraphModel

abstract class CincoPreSaveHook<T extends GraphModel> extends CincoRuntimeBaseClass {

	def abstract void preSave(T object)

}
