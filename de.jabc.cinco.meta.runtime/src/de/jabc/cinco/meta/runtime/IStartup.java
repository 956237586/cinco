package de.jabc.cinco.meta.runtime;

public interface IStartup {
	
	public void startup();
}
