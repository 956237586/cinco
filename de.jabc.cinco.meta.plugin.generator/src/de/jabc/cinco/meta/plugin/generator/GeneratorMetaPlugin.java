package de.jabc.cinco.meta.plugin.generator;

import java.util.List;

import org.eclipse.core.resources.IProject;

import de.jabc.cinco.meta.core.pluginregistry.IMGLMetaPlugin;
import mgl.Annotation;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class GeneratorMetaPlugin implements IMGLMetaPlugin {

	@Override
	public void executeMGLMetaPlugin(List<Annotation> mglAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject) {
		for (var mgl: allMGLs) {
			new CreateCodeGeneratorPlugin().execute(mgl);
		}
	}
	
}
