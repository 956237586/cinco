package de.jabc.cinco.meta.plugin.modelchecking.tmpl.formulas

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.plugin.modelchecking.util.ModelCheckingExtension
import mgl.UserDefinedType

class FormulaFactoryTmpl extends FileTemplate{
	
	extension ModelCheckingExtension = new ModelCheckingExtension
	
	var String expressionAttributeName
	var String descriptionAttributeName
	var String varAttributeName
	var String checkAttributeName = ""
	var UserDefinedType formulasType;
	var boolean checkAttributeExists
	
	override getTargetFileName() '''«graphModel.name»FormulaFactory.java'''
	
	override init(){		
		checkAttributeExists = graphModel.checkAttributeExists
		formulasType = graphModel.formulasType
		expressionAttributeName = graphModel.expressionAttributeName.toFirstUpper
		descriptionAttributeName = graphModel.descriptionAttributeName.toFirstUpper
		varAttributeName = graphModel.varAttributeName.toFirstUpper
		if (checkAttributeExists){
			checkAttributeName = graphModel.checkAttributeName.toFirstUpper
		}	
	}
	
	override template() '''
		package «package»;
		
		import java.util.ArrayList;
		import java.util.List;
		
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula;
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaFactory;
		
		import «graphModel.beanPackage».«model.fileName.toOnlyFirstUpper»Factory;
		
		public class «graphModel.name»FormulaFactory implements FormulaFactory<«graphModel.fqBeanName», «formulasType.fqBeanName»>{
			
			@Override
			public List<CheckFormula> createFormulas(«graphModel.fqBeanName» model) {
				ArrayList<CheckFormula> list = new ArrayList<>();
				for («formulasType.fqBeanName» formula: model.get«graphModel.formulasAttributeName»()) {
					list.add(createFormula(formula));
				}
				
				return list;
			}
		
			@Override
			public CheckFormula createFormula(«formulasType.fqBeanName» formula) {
				CheckFormula newFormula = new CheckFormula(formula.getId(), formula.get«expressionAttributeName»(), formula.get«varAttributeName»());
				newFormula.setDescription(formula.get«descriptionAttributeName»());
				«IF checkAttributeExists»
					newFormula.setToCheck(formula.is«checkAttributeName»());
				«ENDIF»
				return newFormula;
			}
		
			@Override
			public «formulasType.fqBeanName» createGraphModelFormula(String exp, String varName) {
				«model.fileName.toOnlyFirstUpper»Factory factory = «model.fileName.toOnlyFirstUpper»Factory.eINSTANCE;
				«formulasType.fqBeanName» newFormula = factory.create«formulasType.beanName»();
				newFormula.set«expressionAttributeName»(exp);
				newFormula.set«descriptionAttributeName»("");
				newFormula.set«varAttributeName»(varName);
				«IF checkAttributeExists»
					newFormula.set«checkAttributeName»(true);
				«ENDIF»
				return newFormula;
			}
			
		}
		
	'''
	
}
