package de.jabc.cinco.meta.plugin.modelchecking.tmpl.file

import de.jabc.cinco.meta.plugin.modelchecking.util.ModelCheckingExtension
import de.jabc.cinco.meta.plugin.modelchecking.util.PreferencesExtension
import de.jabc.cinco.meta.plugin.template.FileTemplate

class ModelCheckingAdapterTmpl extends FileTemplate{
	
	extension ModelCheckingExtension = new ModelCheckingExtension
	extension PreferencesExtension = new PreferencesExtension
	
	override getTargetFileName() '''«graphModel.name»ModelCheckingAdapter.java'''
	
	override template() '''
		package «package»;
		
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight.HighlightDescription;
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel;
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler;
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.ModelCheckingAdapter;
		import graphmodel.GraphModel;
		import graphmodel.Node;
		
		import java.util.Set;
		
		import «basePackage».builder.«graphModel.name»ModelBuilder;
		«IF graphModel.formulasExist»
		import «basePackage».formulas.«graphModel.name»FormulaFactory;
		import «basePackage».formulas.«graphModel.name»FormulaHandler;
		«ENDIF»
		import «model.package».modelchecking.«graphModel.name.toFirstUpper»ProviderHandler;
		
		public class «graphModel.name»ModelCheckingAdapter implements ModelCheckingAdapter{
			
			@Override
			public boolean canHandle(GraphModel model) {
				return model instanceof «graphModel.fqBeanName»;
			}
		
			@Override
			public void buildCheckableModel(GraphModel model, CheckableModel<?,?> checkableModel, boolean withSelection) {
				if (canHandle(model)) {
					«graphModel.name»ModelBuilder builder = new «graphModel.name»ModelBuilder();
					builder.buildModel((«graphModel.fqBeanName») model, checkableModel, withSelection);
				}
			}
			
			«IF graphModel.formulasExist»
			@Override
			public FormulaHandler<«graphModel.fqBeanName», «graphModel.formulasType.fqBeanName»> getFormulaHandler() {
				return new «graphModel.name»FormulaHandler(new «graphModel.name»FormulaFactory());
			}
			
			@Override
			public Class<?> getFormulasTypeClass(){
				return «graphModel.beanPackage».impl.«graphModel.formulasTypeName»Impl.class; 
			}
			«ELSE»
			@Override
			public FormulaHandler<«graphModel.fqBeanName»,?> getFormulaHandler() {
				return null;
			}
			
			@Override
			public Class<?> getFormulasTypeClass(){
				return null; 
			}
			«ENDIF»
			
			@Override 
			public String getSettings(String key){
				switch(key){
					«FOR setting:graphModel.getAllPreferences.entrySet»
					case "«setting.key»": return "«setting.value»";
					«ENDFOR»
					default: return null;
				}
			}
			
			@Override
			public boolean fulfills(GraphModel model, CheckableModel<?,?> checkableModel, Set<Node> satisfyingNodes) {
				if(canHandle(model)) {
					return (new «graphModel.name.toFirstUpper»ProviderHandler()).fulfills((«graphModel.fqBeanName») model, checkableModel, satisfyingNodes);
				}
				return false;
			}
			
			@Override
			public Set<HighlightDescription> getHighlightDescriptions(GraphModel model, Set<HighlightDescription> intendedHighlightDescriptions){
				if(canHandle(model)) {
					return (new «graphModel.name.toFirstUpper»ProviderHandler()).getHighlightDescriptions((«graphModel.fqBeanName») model, intendedHighlightDescriptions);
				}
				return intendedHighlightDescriptions;
			}
		}
		
	'''
	
}
