package de.jabc.cinco.meta.plugin.modelchecking.provider

import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.swt.widgets.ColorDialog
import org.eclipse.swt.widgets.Shell

class PickColorReplacementApplier extends ReplacementTextApplier{
	val String key
	val String operator
	val String proposal
	
	new(String proposal, String key, String operator) {
		this.key = key
		this.operator = operator
		this.proposal = proposal
	}
	
	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
		if (proposal.displayString == this.proposal){
			val cDialog = new ColorDialog(new Shell)
			val rgb = cDialog.open
			
			val r = String.valueOf(rgb.red)
			val g = String.valueOf(rgb.green)
			val b = String.valueOf(rgb.blue)
			
			return '''"«key» «operator» («r»,«g»,«b»)"'''
		}
		return proposal.displayString	
	}
}