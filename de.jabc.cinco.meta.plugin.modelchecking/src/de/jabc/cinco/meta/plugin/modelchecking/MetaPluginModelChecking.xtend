package de.jabc.cinco.meta.plugin.modelchecking

import de.jabc.cinco.meta.plugin.CincoMetaPlugin
import de.jabc.cinco.meta.plugin.modelchecking.tmpl.project.GraphModelProjectTmpl
import de.jabc.cinco.meta.plugin.modelchecking.tmpl.project.ModelCheckingProjectTmpl

class MetaPluginModelChecking extends CincoMetaPlugin{
		
	override executeCincoMetaPlugin() {
		for (mgl: generatedMGLs) {
			for (gm: mgl.graphModels) {
				new ModelCheckingProjectTmpl => [
					model = mgl
					graphModel = gm
					create
				]
				new GraphModelProjectTmpl => [
					model = mgl
					graphModel = gm
					create
				]
			}
		}
	}
	
}