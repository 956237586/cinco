package de.jabc.cinco.meta.plugin.modelchecking.util

import java.util.Map
import mgl.GraphModel
import java.util.Map.Entry
import mgl.Annotation

class PreferencesExtension {

	public val String PREFERENCES_OPERATOR = ":="

	val Map<String, String> settingsMap

	new() {
		settingsMap = newHashMap
		settingsMap.putDefaultValues
	}

	def putDefaultValues(Map<String, String> it) {
		put("varNamePrefix", "%")
		put("defaultModelChecker", "GEAR")
		put("highlightColor", "(0,255,0)")
		put("selectionMode", "false")
		put("highlightNodes", "true")
		put("highlightSubFormulas", "true")
		put("autoCheck", "true")
		put("showDescription", "false")
	}

	def isValidValue(String key, String value) {
		val valid = key.validValues
		valid.empty || valid.contains(value)
	}

	def getValidValues(String key) {
		switch (key) {
			case "selectionMode",
			case "highlightNodes",
			case "highlightSubFormulas",
			case "autoCheck",
			case "showDescription": #{"true", "false"}
			default: #{}
		}
	}

	def getDefaultPreferences(String key) {
		val map = newHashMap
		putDefaultValues(map)
		map.get(key)
	}

	def getPreferenceKeys() {
		settingsMap.keySet
	}

	def getKey(String annotationValue) {
		val pos = annotationValue.indexOf(PREFERENCES_OPERATOR)
		if (pos == -1){
			return null
		}
		return annotationValue.substring(0, pos).trim
	}

	def getValue(String annotationValue) {
		val pos = annotationValue.indexOf(PREFERENCES_OPERATOR)
		if (pos == -1){
			return null
		}
		return annotationValue.substring(pos + PREFERENCES_OPERATOR.length).trim
	}

	def getPreference(GraphModel model, String key) {
		model.getAllPreferences.get(key)
	}

	def getAllPreferences(GraphModel model) {
		val manualSettings = model.getManualPreferences
		val allSettings = newHashMap
		for (key : preferenceKeys) {
			val value = manualSettings.get(key)
			if (value !== null && key.isValidValue(value)) {
				allSettings.put(key, value)
			} else {
				allSettings.put(key, key.defaultPreferences)
			}
		}
		allSettings
	}

	def Map<String, String> getManualPreferences(GraphModel model) {
		val settingsMap = newHashMap
		model.annotations.filter[name == new ModelCheckingExtension().ANNOTATION_PREFERENCES].forEach [
			settingsMap.putAll(getManualPreferences)
		]
		settingsMap
	}

	def getManualPreferences(Annotation annotation) {
		val settings = annotation?.value

		val settingsMap = newHashMap
		if (settings !== null) {
			for (it : settings) {
				val entry = getManualPreference
				if (entry !== null){
					settingsMap.put(entry.key, entry.value)
				}				
			}
		}
		return settingsMap
	}

	def getManualPreference(String annotationValue) {
		if (annotationValue !== null) {
			
			val tmpVal = annotationValue.value
			
			if (tmpVal === null){
				return null
			}
			
			val tmpKey = annotationValue.key
			
			if (tmpKey === null){
				return null
			}
			
			return new Entry<String, String> {

				var entryValue = tmpVal

				override getKey() { return tmpKey }

				override getValue() { entryValue }

				override setValue(String value) {
					this.entryValue = value
				}
			}
		}
	}
}
