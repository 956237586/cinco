package de.jabc.cinco.meta.plugin.modelchecking.provider

import de.jabc.cinco.meta.core.pluginregistry.proposalprovider.IMetaPluginAcceptor
import mgl.Annotation
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FulfillmentConstraint
import de.jabc.cinco.meta.plugin.modelchecking.util.PreferencesExtension
import mgl.GraphModel
import de.jabc.cinco.meta.plugin.modelchecking.util.ModelCheckingExtension

class ModelCheckingProposalProvider implements IMetaPluginAcceptor{
	
	extension PreferencesExtension = new PreferencesExtension
	extension ModelCheckingExtension = new ModelCheckingExtension
	
	val String PICK_HIGHLIGHT_COLOR = "Pick highlight color..."
	
	override getAcceptedStrings(Annotation annotation) {
		switch (annotation.name){
			case ANNOTATION_DEFAULT, case ANNOTATION_MC: return #["include","exclude"]
			case ANNOTATION_FULFILLMENT: return FulfillmentConstraint.values.map[toString]
			case ANNOTATION_PREFERENCES: return annotation.possiblePreferences
			default : return #[]
		}
	}
	
	def getPossiblePreferences(Annotation annotation){
		val prefs = newHashSet
		val model = annotation.annotatedModelElement as GraphModel
		val currentKeys = model.manualPreferences.keySet
		preferenceKeys.filter[!currentKeys.contains(it)].forEach[key |
			val valid = key.validValues
			if (valid.empty){
				if (key != "highlightColor"){
					prefs.add("\"" + key + " " + PREFERENCES_OPERATOR + " \"")
				}else{
					prefs.add(PICK_HIGHLIGHT_COLOR)
				}
			}else {
				valid.forEach[value |
						prefs.add("\"" + key + " " + PREFERENCES_OPERATOR + " " + value + "\"")
				]
			}
		]
		return prefs.toList
	}
	
	override getTextApplier(Annotation annotation) {
		if (annotation.name == ANNOTATION_PREFERENCES){
			return new PickColorReplacementApplier(PICK_HIGHLIGHT_COLOR, "highlightColor", PREFERENCES_OPERATOR)
		}
		null
	}
}