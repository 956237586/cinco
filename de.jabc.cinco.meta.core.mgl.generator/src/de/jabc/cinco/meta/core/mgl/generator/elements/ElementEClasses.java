package de.jabc.cinco.meta.core.mgl.generator.elements;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EcoreFactory;

import mgl.ModelElement;

public class ElementEClasses {
	private EClass mainEClass = null;
	private EClass internalEClass = null;
	private EClass mainView = null;
	private ModelElement modelElement;
	
	public ElementEClasses(ModelElement modelElement) {
		this.modelElement = modelElement;
		this.mainEClass = EcoreFactory.eINSTANCE.createEClass();
		this.mainEClass.setName(modelElement.getName());
		this.internalEClass = EcoreFactory.eINSTANCE.createEClass();
		this.internalEClass.setName("Internal" + modelElement.getName());
		this.mainView = EcoreFactory.eINSTANCE.createEClass();
		this.mainView.setName(modelElement.getName() + "View");
	}

	public EClass getMainEClass() {
		return mainEClass;
	}

	public void setMainEClass(EClass mainEClass) {
		this.mainEClass = mainEClass;
	}

	public EClass getInternalEClass() {
		return internalEClass;
	}

	public void setInternalEClass(EClass internalEClass) {
		this.internalEClass = internalEClass;
	}

	public EClass getMainView() {
		return mainView;
	}

	public void setMainView(EClass mainView) {
		this.mainView = mainView;
	}

	public ModelElement getModelElement() {
	  return this.modelElement;
	}
	
	public void setModelElement(ModelElement modelElement){
		this.modelElement = modelElement;
	}
	
}
