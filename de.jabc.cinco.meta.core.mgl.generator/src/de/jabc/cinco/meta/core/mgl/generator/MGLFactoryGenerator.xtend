package de.jabc.cinco.meta.core.mgl.generator

import mgl.MGLModel
import de.jabc.cinco.meta.core.mgl.generator.elements.ElementEClasses
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.util.xapi.WorkspaceExtension
import org.eclipse.core.runtime.Path

import static extension de.jabc.cinco.meta.core.mgl.generator.extensions.FactoryGeneratorExtensions.*
import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.util.xapi.FileExtension

class MGLFactoryGenerator {
	
	extension FileExtension = new FileExtension
	extension GeneratorUtils = GeneratorUtils.instance
	
	def generateFactory(MGLModel model, Iterable<ElementEClasses> elmClasses) {
		val factoryContent = model.createFactory(elmClasses, getAllImportedMGLs(model, true, false))
		
		val path = "/src-gen/" + model.package.replaceAll("\\.", "/") + "/factory"
		val name = model.fileName + "Factory.xtend"
		val project = ProjectCreator.getProject(model.eResource)
		val packageName = model.package
		ProjectCreator.exportPackage(project, packageName+".factory")
		val fullPath = path + "/" + name
		val file = project.getFile(fullPath)
		if (!file.exists) {
			val we = new WorkspaceExtension()
			we.createFolders(project, new Path(path))
			we.create(file)
		}
		file.writeContent(factoryContent.toString)
	}
	
}