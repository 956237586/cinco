package de.jabc.cinco.meta.core.mgl.generator

import mgl.MGLModel
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.core.utils.projects.ContentWriter
import de.jabc.cinco.meta.core.mgl.generator.extensions.AdapterGeneratorExtension
import mgl.UserDefinedType

class MGLAdaptersGenerator {
	
	extension AdapterGeneratorExtension = new AdapterGeneratorExtension
	
	def generateAdapters(MGLModel model) {
		val project = ProjectCreator.getProject(model.eResource)
		val packageName = model.package + ".adapter"
		ProjectCreator.exportPackage(project,packageName)
		
		for(n: model.graphModels.filter[!isAbstract]){
			val adapterContent = n.generateAdapter
			val fileName = n.name + "EContentAdapter.xtend"
			ContentWriter::writeFile(project, "src-gen", packageName, fileName, adapterContent.toString)
		}
		for (n : model.nodes + model.edges) {
			val fileName = n.name + "EContentAdapter.xtend"
			val adapterContent = n.generateAdapter
			ContentWriter::writeFile(project, "src-gen", packageName, fileName, adapterContent.toString)
		}
		for (t : model.types.filter(UserDefinedType)) {
			val fileName = t.name + "EContentAdapter.xtend"
			val adapterContent = t.generateAdapter
			ContentWriter::writeFile(project, "src-gen", packageName, fileName, adapterContent.toString)
		}
	}
	
}