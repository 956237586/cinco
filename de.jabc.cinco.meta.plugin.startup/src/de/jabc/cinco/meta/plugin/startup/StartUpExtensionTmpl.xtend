package de.jabc.cinco.meta.plugin.startup

import mgl.MGLModel

class StartUpExtensionTmpl {
	
	static def extensionEntry(MGLModel mgl,String startupClazz)	'''
	<extension point="de.jabc.cinco.meta.runtime.startup">
	<startupclass cincoProductID="«mgl.package»"
			class="«startupClazz»">
		</startupclass>
	</extension>
	'''   
}