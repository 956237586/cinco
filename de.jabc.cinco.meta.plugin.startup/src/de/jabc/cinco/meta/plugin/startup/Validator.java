package de.jabc.cinco.meta.plugin.startup;

import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult;
import de.jabc.cinco.meta.plugin.validation.SimpleValidator;
import mgl.Annotation;

public class Validator extends SimpleValidator {
		Pattern pattern;
	public Validator() {
		String regex = "[a-zA-Z$][A-Za-z0-9_$]*(\\.[a-zA-Z$][A-Za-z0-9_$]*)*";
		
		pattern = Pattern.compile(regex);
	}

	@Override
	public String getProjectAnnotation() {
		
		return "startup";
	}

	@Override
	public ValidationResult<String, EStructuralFeature> checkProjectAnnotation(Annotation annot) {
		if(annot.getValue().size()<1){
			return newError("'@startup' Annotation must have at least 1 value'");
		}else {
			for(String value: annot.getValue()) {
				if(!pattern.matcher(value).matches()) {
					return newError(String.format("'%s' is not a valid fully qualified Class Name", value));
				}
			}
			
		}
		
		return super.checkProjectAnnotation(annot);
	}

}
