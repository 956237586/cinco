package de.jabc.cinco.meta.plugin.startup;

import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.addAttribute;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.addExtension;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.addSubElement;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.openPluginXMLDocument;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.save;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin;
import mgl.MGLModel;
import productDefinition.Annotation;
import productDefinition.CincoProduct;

public class StartUpMetaPlugin implements ICPDMetaPlugin {

	public StartUpMetaPlugin() {
		// Intentionally left blank
	}
	
	@Override
	public void executeCPDMetaPlugin(List<Annotation> cpdAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject) {
		String projectName = mainProject.getName();
		for (var annotation: cpdAnnotations) {
			if (annotation.getName().equals("startup")) {
				Document pluginXML = openPluginXMLDocument(mainProject);
				for (String value: annotation.getValue()) {
					Element extension = addExtension(pluginXML, "de.jabc.cinco.meta.runtime.startup");
					Element startupClass = addSubElement(extension, "startupclass");
					addAttribute(startupClass, "cincoProductID", projectName);
					addAttribute(startupClass, "class", value);
				}
				save(pluginXML, mainProject, new NullProgressMonitor());
			}
		}
	}
	
}
