package de.jabc.cinco.meta.core.ge.style.generator.api.main

import de.jabc.cinco.meta.core.ge.style.generator.api.templates.CEdgeTmpl
import de.jabc.cinco.meta.core.ge.style.generator.api.templates.CGraphModelTmpl
import de.jabc.cinco.meta.core.ge.style.generator.api.templates.CNodeTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.core.utils.projects.ContentWriter
import mgl.MGLModel
import mgl.ModelElement
import org.eclipse.core.resources.IProject

class CincoApiGeneratorMain extends APIUtils {
	
	extension CNodeTmpl = new CNodeTmpl
	extension CEdgeTmpl = new CEdgeTmpl
	extension CGraphModelTmpl = new CGraphModelTmpl
	extension GeneratorUtils = GeneratorUtils.instance
	
	var MGLModel mgl
	
	new (MGLModel mglModel) {
		this.mgl = mglModel
	}
	
	def mglModel(ModelElement m){
		m.eContainer as MGLModel
	}
	
	def doGenerate(IProject project) {
		var CharSequence content = null
		
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			content = gm.doGenerateImpl
			ContentWriter::writeJavaFileInSrcGen(project, mgl.packageNameAPI, gm.fuCName.concat(".java"), content)
		}
		
		for (n : mgl.nodes) {
			content = n.doGenerateImpl
			ContentWriter::writeJavaFileInSrcGen(project, mgl.packageNameAPI, n.fuCName.concat(".java"), content)
		}
		
		for (e : mgl.edges) {
			content = e.doGenerateImpl
			ContentWriter::writeJavaFileInSrcGen(project, mgl.packageNameAPI, e.fuCName.concat(".java"), content)
		}
	}
	
}