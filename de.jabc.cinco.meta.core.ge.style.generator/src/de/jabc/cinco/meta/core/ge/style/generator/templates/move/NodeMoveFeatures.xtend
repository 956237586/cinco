package de.jabc.cinco.meta.core.ge.style.generator.templates.move

import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.ECincoError
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoMoveShapeFeature
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import graphmodel.internal.InternalModelElementContainer
import mgl.ModelElement
import mgl.Node
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.IMoveShapeContext
import style.Styles

class NodeMoveFeatures {
	
	extension APIUtils = new APIUtils
	extension GeneratorUtils = GeneratorUtils.instance
	
	/**
	 * Generates the 'Move-Feature' for a given node
	 * @param n : The node
	 * @param style : The style
	 */
	def doGenerateNodeMoveFeature(Node n, Styles styles)'''
		package «n.packageNameMove»;
		
		class MoveFeature«n.fuName» extends «CincoMoveShapeFeature.name» {
			
			private «ECincoError.name» error = «ECincoError.name».OK
			
			/**
			 * Call of the Superclass
			 * @param fp: Fp is the parameter of the Superclass-Call
			*/
			new(«IFeatureProvider.name» fp) {
				super(fp)
			}
			
			/**
			 * Checks if a shape is moveable
			 * @param context : Contains the information, needed to let a feature move a shape
			 * @param apiCall : Apicall shows if the Cinco Api is used
			 * @return Returns true if a shape can be moved and false if not
			*/
			override boolean canMoveShape(«IMoveShapeContext.name» context, boolean apiCall) {
				if (apiCall) {
					val o = getBusinessObjectForPictogramElement(context.getShape())
					val source = getBusinessObjectForPictogramElement(context.getSourceContainer())
					val target = getBusinessObjectForPictogramElement(context.getTargetContainer())
					«IF n.isFixed(styles)»
					return false
					«ELSE»
					if (source !== null && source.equals(target))
						return true
					if (target instanceof «InternalModelElementContainer.name»)
						return (target as «InternalModelElementContainer.name»).canContain(«n.fqBeanName»)
					if (getError().equals(«ECincoError.name».OK))
						setError(«ECincoError.name».INVALID_CONTAINER)
					return false
					«ENDIF»
				}
				return false;
			}
			
			/**
			 * Checks if a shape is moveable by using the method 'canMoveShape(context,apiCall)'
			 * @param context : Contains the information, needed to let a feature move a shape
			 * @return Returns true if a shape can be moved and false if not
			*/
			override boolean canMoveShape(«IMoveShapeContext.name» context) {
				return canMoveShape(context, «!CincoUtil.isMoveDisabled(n)»)
			}
		
			/**
			 * Moves a Shape by removing the shape at the source and adding it at the target
			 * @param context : Contains the information, needed to let a feature move a shape
			*/
			override void moveShape(«IMoveShapeContext.name» context) {
				val o = getBusinessObjectForPictogramElement(context.getShape()) as «n.fqInternalBeanName»
				val target = getBusinessObjectForPictogramElement(context.getTargetContainer()) as «InternalModelElementContainer.name»
				
				super.moveShape(context)
				
				if (o.getElement() instanceof «n.fqCName») {
					if(!moveContainer(target, o.getElement() as «n.fqBeanName», context)) {
						(o.getElement() as «n.fqBeanName»)
							.moveTo(target.getContainerElement(), context.getX(), context.getY())
					}
				}
			}
			
			def dispatch moveContainer(«InternalModelElementContainer.name» target, «n.fqBeanName» oe, «IMoveShapeContext.name» context) {
				return false
			}
			
			«FOR pc : n.possibleContainers»
				def dispatch moveContainer(«(pc as ModelElement).fqInternalBeanName» target, «n.fqBeanName» oe, «IMoveShapeContext.name» context) {
					oe.moveTo(target.getContainerElement() as «pc.fqBeanName», context.getX(), context.getY())
					return true
				}
			«ENDFOR»
			
			/**
			 * Get-method for an error
			 * @return Returns an 'error' in which 'error' is  'ECincoError.OK'
			*/
			override «ECincoError.name» getError() {
				return error
			}

			/**
			 * Set-method for an error
			 * @param error : Error is a value of the enum: MAX_CARDINALITY, MAX_IN, MAX_OUT, INVALID_SOURCE, INVALID_TARGET, INVALID_CONTAINER, INVALID_CLONE_TARGET, OK
			*/
			def void setError(«ECincoError.name» error) {
				this.error = error
			}
			
		}
	'''
	/**
	 * Auxiliary method to check if a style of a node is fixed
	 * @param n : The node
	 * @param style : The style
	 * @return Returns true if the style of a node is fixed and false if not
	 */
	def isFixed(Node n, Styles styles)
	{
		var style = CincoUtil.getStyleForNode(n, styles);
		return style.fixed;
	}
}