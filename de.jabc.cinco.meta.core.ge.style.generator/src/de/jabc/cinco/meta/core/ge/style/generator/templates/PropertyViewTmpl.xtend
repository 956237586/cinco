package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.ui.properties.CincoPropertyView
import de.jabc.cinco.meta.core.ui.properties.IValuesProposalProvider
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import mgl.Edge
import mgl.GraphModel
import mgl.UserDefinedType
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

import static extension de.jabc.cinco.meta.core.utils.CincoUtil.*
import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class PropertyViewTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension APIUtils = new APIUtils
	extension EventApiExtension = new EventApiExtension

	/**
	 * Generates the code which registers the {@link EStructuralFeature}s for the 
	 * generic {@link CincoPropertyView}
	 * 
	 * @param gm The processed {@link GraphModel}
	 */	
	def generatePropertyView(GraphModel gm)'''
		package «gm.packageName».property.view;
		
		import java.util.function.Consumer;
		import org.eclipse.emf.ecore.EObject;
		
		public class «gm.fuName»PropertyView implements «IValuesProposalProvider.name» {
			
			«FOR me: gm.modelElements.filter[findAnnotationPostSelect !== null]»
				«val postSelectClass = me.findAnnotationPostSelect.value.head»
				static Consumer<EObject> postSelect«me.fuName» = (bo) -> {
					if (bo instanceof «me.fqBeanName») {
						new «postSelectClass»().postSelect((«me.fqBeanName») bo);
					}
				};
			«ENDFOR»
			
			static Consumer<EObject> eventPostSelect = (bo) -> {
				«gm.getEventEnabledElements(EventEnum.POST_SELECT)
					.ancestorElements
					.sortByInheritance
					.reverse
					.ifElseCascade(
						[ me | me.instanceofCheck('bo') ],
						[ me | '''
							«me.fqBeanName» element = («me.fqBeanName») bo;
							if (element.getRootElement() instanceof «gm.fqCName») {
								«EventEnum.POST_SELECT.getNotifyCallJava(me, 'element')»
							}
						''']
					)
				»
			};
			
			static Consumer<EObject> refreshPossibleValues = (bo) -> new «gm.fuName»PropertyView().refreshValues(bo);
			
			public static void initEStructuralFeatureInformation(){
				
				«CincoPropertyView.name».init_EStructuralFeatures(«gm.beanPackage».internal.impl.Internal«gm.fuName»Impl.class, 
					new «EStructuralFeature.name»[] {
						«FOR attr : gm.attributes.filter[!isAttributeHidden] SEPARATOR ","»
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«gm.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»
					}
				);
				
				«val usableNodes = getUsableNodes(gm, true)»
				«val usableNodesAndSuperNodes = (usableNodes + usableNodes.flatMap[allSuperNodes]).toSet»
				«val usableEdges = getUsableEdges(gm)»
				«val usableEdgesAndSuperEdges = (usableEdges + usableEdges.map[allSuperEdges].flatten).toSet»
				«FOR e : (usableNodesAndSuperNodes + usableEdgesAndSuperEdges)»
					«CincoPropertyView.name».init_EStructuralFeatures(
						«e.beanPackage».internal.impl.Internal«e.fuName»Impl.class,
							new «EStructuralFeature.name»[] {
								«val allNonHiddenAttributes = e.allAttributes(false).filter[!isAttributeHidden]»
								«FOR attr : allNonHiddenAttributes SEPARATOR ","»
									«e.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«e.fuName»_«attr.name.toFirstUpper»()
								«ENDFOR»
							}
					);
				«ENDFOR»
				
				«FOR t : gm.types.filter[t | t instanceof UserDefinedType].map[t | t as UserDefinedType]»
					«CincoPropertyView.name».init_EStructuralFeatures(
						«t.beanPackage».internal.impl.Internal«t.fuName»Impl.class, 
						new «EStructuralFeature.name»[] {
							«val allNonHiddenAttributes = t.allAttributes(false).filter[!isAttributeHidden]»
							«FOR attr : allNonHiddenAttributes SEPARATOR ","»
								«t.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«t.fuName»_«attr.name.toFirstUpper»()
							«ENDFOR»
							«val allSuperTypes = t.allSuperTypes.filter[it instanceof Edge]»
							«IF !allNonHiddenAttributes.nullOrEmpty && !allSuperTypes.nullOrEmpty»,«ENDIF»
							«FOR parent : allSuperTypes SEPARATOR ","»
								«FOR attr : parent.allAttributes(false).filter[!isAttributeHidden] SEPARATOR ","»
									«parent.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«parent.fuName»_«attr.name.toFirstUpper»()
								«ENDFOR»
							«ENDFOR»
						}
					);
				«ENDFOR»
		
				«CincoPropertyView.name».init_MultiLineAttributes(new «EStructuralFeature.name»[] {
					«FOR attr : gm.allModelAttributes.filter[isAttributeMultiLine] SEPARATOR ","»
						«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»()
					«ENDFOR»			
				});
		
				«CincoPropertyView.name».init_ReadOnlyAttributes(new «EStructuralFeature.name»[] {
					«FOR me : gm.modelElements.filter[allAttributes(false).exists[isAttributeReadOnly]] SEPARATOR ","»
						«FOR attr : me.allAttributes(false).filter[isAttributeReadOnly] SEPARATOR ","»
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«me.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»
					«ENDFOR»
				});
				
				«CincoPropertyView.name».init_FileAttributes(new «EStructuralFeature.name»[] {
					«FOR attr : gm.allModelAttributes.filter[isAttributeFile] SEPARATOR ","»
						«FOR subtype : attr.modelElement.allSubclasses + #[attr.modelElement] SEPARATOR ","»
							«subtype.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«subtype.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»
					«ENDFOR»                        
				});
				
				«IF gm.allModelAttributes.exists[isAttributeFile]»
					«FOR attr : gm.allModelAttributes.filter[isAttributeFile]»
						«FOR subtype : attr.modelElement.allSubclasses + #[attr.modelElement]»
							«CincoPropertyView.name».init_FileAttributesExtensionFilters(
								«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«subtype.fuName»_«attr.name.toFirstUpper»(),
								new String[] {"«attr.annotations.filter[name == "file"].map[value].flatten.join("\",\"")»"}
							);
						«ENDFOR»
					«ENDFOR»
				«ENDIF»
			
				«FOR t : gm.types.filter[t | t instanceof UserDefinedType].map[t | t as UserDefinedType]»
					«CincoPropertyView.name».init_ColorAttributes(new «EStructuralFeature.name»[] {
						«FOR attr :t.allAttributes.filter[isAttributeColor] SEPARATOR ","»
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«t.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»                        
					});
				«ENDFOR»
		
				«FOR n : gm.nodes»
					«CincoPropertyView.name».init_ColorAttributes(new «EStructuralFeature.name»[] {
						«FOR attr :n.allAttributes.filter[isAttributeColor] SEPARATOR ","»
							«n.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»                        
					});
				«ENDFOR»
				
				«FOR e : gm.edges»
					«CincoPropertyView.name».init_ColorAttributes(new «EStructuralFeature.name»[] {
						«FOR attr : e.allAttributes.filter[isAttributeColor] SEPARATOR ","»
							«e.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»                        
					});
				«ENDFOR»
				
				«FOR t : gm.types.filter[t| t instanceof UserDefinedType].map[t| t as UserDefinedType]»
					«IF gm.allModelAttributes.exists[isAttributeColor]»
						«FOR attr : t.allAttributes.filter[isAttributeColor]»
							«CincoPropertyView.name».init_ColorAttributesParameter(
								«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«t.fuName»_«attr.name.toFirstUpper»(),
								"«attr.annotations.filter[name == "color"].map[value].get(0).get(0)»"
							);
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
				
				«FOR n : gm.nodes»
					«IF gm.allModelAttributes.exists[isAttributeColor]»
						«FOR attr : n.allAttributes.filter[isAttributeColor]»
							«CincoPropertyView.name».init_ColorAttributesParameter(
								«n.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»(),
								"«attr.annotations.filter[name == "color"].map[value].get(0).get(0)»"
							);
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
		
				«FOR e : gm.edges»
					«IF gm.allModelAttributes.exists[isAttributeColor]»
						«FOR attr : e.allAttributes.filter[isAttributeColor]»
							«CincoPropertyView.name».init_ColorAttributesParameter(
								«e.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»(),
								"«attr.annotations.filter[name == "color"].map[value].get(0).get(0)»"
							);
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
				
				«FOR t : gm.types.filter(UserDefinedType).filter[hasLabel]»
					«CincoPropertyView.name».init_TypeLabel(
						«gm.beanPackage».internal.impl.Internal«t.fuName»Impl.class,
						«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«t.fuName»_«t.annotations.filter[name == "label"].head.value.head.toFirstUpper»()
					);
				«ENDFOR»
				
				«FOR attr : gm.allModelAttributes.filter[isGrammarAttribute]»
					«FOR subType : attr.modelElement.allSubclasses + #[attr.modelElement]»
						«CincoPropertyView.name».init_GrammarEditor(
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«subType.fuName»_«attr.name.toFirstUpper»(),
							«attr.annotations.filter[name == "grammar"].head.value.get(1)».getInstance().getInjector("«attr.annotations.filter[name == "grammar"].head.value.get(0)»")
						);
					«ENDFOR»
				«ENDFOR»
				
				«FOR me: gm.modelElements.filter[findAnnotationPostSelect !== null]»
					«CincoPropertyView.name».postSelects.add(postSelect«me.fuName»);
				«ENDFOR»
				
				«CincoPropertyView.name».postSelects.add(eventPostSelect);
				
				«CincoPropertyView.name».possibleValueRefreshs.add(refreshPossibleValues);
				
				«CincoPropertyView.name».assertSelectionListener();
			}
			
			@Override
			public void refreshValues(«EObject.name» bo) {
				«FOR attr : gm.allModelAttributes.filter[isAttributePossibleValuesProvider]»
					«FOR element : attr.modelElement.allSubclasses + #{attr.modelElement}»
						if (bo instanceof «attr.modelElement.fqBeanName»)
							«CincoPropertyView.name».refreshPossibleValues(
								«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.name»_«attr.name.toFirstUpper»(),
								new «attr.getPossibleValuesProviderClass»().getPossibleValues((«attr.modelElement.fqBeanName») bo)
							);
					«ENDFOR»
				«ENDFOR»
			}
			
		}
	'''
	
}