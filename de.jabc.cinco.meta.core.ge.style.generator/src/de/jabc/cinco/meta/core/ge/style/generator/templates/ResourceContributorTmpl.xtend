package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import mgl.GraphModel

class ResourceContributorTmpl {
	
	protected extension GeneratorUtils = GeneratorUtils.instance
	
	def generateResourceContributor(GraphModel gm) '''
		package «gm.packageName»;
		
		import java.util.ArrayList;
		import java.util.Collections;
		
		import org.eclipse.emf.ecore.EObject;
		import org.eclipse.emf.ecore.resource.Resource;
		
		import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement;
		import de.jabc.cinco.meta.core.ge.style.generator.runtime.editor.DiagramBuilder;
		import de.jabc.cinco.meta.core.ge.style.generator.runtime.editor.LazyDiagram;
		import de.jabc.cinco.meta.core.ui.editor.ResourceContributor;
		import de.jabc.cinco.meta.runtime.xapi.ResourceExtension;
		import graphmodel.GraphModel;
		import graphmodel.internal.InternalGraphModel;
		import «gm.fqBeanName»;
		
		public class «gm.fuName»ResourceContributor implements ResourceContributor {
			
			ResourceExtension resourceHelper = new ResourceExtension();
			
			@Override
			public Iterable<EObject> contributeToResource(Resource resource) {
				@SuppressWarnings("restriction")
				InternalGraphModel internalModel = resourceHelper.getContent(resource, InternalGraphModel.class);
				GraphModel model = (internalModel != null) ? internalModel.getElement() : null;
				if (model.getClass().getSimpleName().substring(1).equals("«gm.fuName»") || model.getClass().getSimpleName().equals("«gm.fuName»")) {
					LazyDiagram diagram = new DiagramBuilder(new «gm.fuName»Diagram(), model).build(resource);
					if (model instanceof CModelElement) {
						((CModelElement) model).setPictogramElement(diagram);
					}
					ArrayList<EObject> contributions = new ArrayList<>();
					contributions.add(diagram);
					return contributions;
				}
				return Collections.emptyList();
			}
		
			@Override
			public boolean isResolveCrossReferencesRequired(EObject obj) {
				return false;
			}
		}
	'''
}