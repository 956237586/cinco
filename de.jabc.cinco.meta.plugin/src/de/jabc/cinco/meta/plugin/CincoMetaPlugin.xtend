package de.jabc.cinco.meta.plugin

import de.jabc.cinco.meta.core.BundleRegistry
import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin
import de.jabc.cinco.meta.core.pluginregistry.IMGLMetaPlugin
import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import java.util.List
import mgl.MGLModel
import org.eclipse.core.resources.IProject
import productDefinition.CincoProduct

abstract class CincoMetaPlugin extends CincoMetaContext implements IMGLMetaPlugin, ICPDMetaPlugin {
	
	abstract def void executeCincoMetaPlugin()
	
	override executeMGLMetaPlugin(List<mgl.Annotation> mglAnnotations,
	                              List<MGLModel> generatedMGLs,
	                              List<MGLModel> allMGLs,
	                              CincoProduct cpd,
	                              IProject mainProject) {
		try {
			this.mglAnnotations = mglAnnotations
			this.generatedMGLs  = generatedMGLs
			this.allMGLs        = allMGLs
			this.cpd            = cpd
			this.mainProject    = mainProject
			executeCincoMetaPlugin
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
	override executeCPDMetaPlugin(List<productDefinition.Annotation> cpdAnnotations,
	                              List<MGLModel> generatedMGLs,
	                              List<MGLModel> allMGLs,
	                              CincoProduct cpd,
	                              IProject mainProject) {
		try {
			this.cpdAnnotations = cpdAnnotations
			this.generatedMGLs  = generatedMGLs
			this.allMGLs        = allMGLs
			this.cpd            = cpd
			this.mainProject    = mainProject
			executeCincoMetaPlugin
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
	def IProject create(ProjectTemplate projectTemplate) {
		projectTemplate.setContextIfNotAlreadySet(this)
		val project = projectTemplate.createProject()
		if (project !== null) {
			BundleRegistry.INSTANCE.addBundle(project.name, false)
		}
		return project
	}
	
	def <T extends ProjectTemplate> IProject create(Class<T> projectTemplateClass) {
		val projectTemplate = projectTemplateClass
			.constructors
			.findFirst[parameterCount == 0]
			.newInstance as T
		return create(projectTemplate)
	}
	
}
