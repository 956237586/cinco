package de.jabc.cinco.meta.plugin.template

import de.jabc.cinco.meta.plugin.CincoMetaContext
import de.jabc.cinco.meta.plugin.dsl.ProjectDescription
import de.jabc.cinco.meta.plugin.dsl.ProjectDescriptionLanguage

abstract class ProjectTemplate extends CincoMetaContext {
	
	extension protected ProjectDescriptionLanguage = new ProjectDescriptionLanguage
	
	var String projectName
	var ProjectDescription projectDescription
	
	abstract def protected ProjectDescription _projectDescription()
	abstract def protected String _projectName()
	
	def void init() {
		// Override in sub classes
	}
	
	def createProject() {
		init
		getProjectDescription()?.withContext(this).create()
	}
	
	def getProjectDescription() {
		if (projectDescription === null) {
			projectDescription = _projectDescription()
		}
		return projectDescription
	}
	
	def protected getProjectDescription(Class<? extends ProjectTemplate> templateClass) {
		templateClass.newInstanceWithContext(this).getProjectDescription()
	}
	
	def getProjectName() {
		if (projectName === null) {
			projectName = _projectName()
		}
		return projectName
	}
	
	def protected getProjectName(Class<? extends ProjectTemplate> templateClass) {
		templateClass.newInstanceWithContext(this).getProjectName()
	}
	
	def getBasePackage() {
		getProjectName()
	}
	
	def protected getBasePackage(Class<? extends ProjectTemplate> templateClass) {
		templateClass.newInstanceWithContext(this).getBasePackage()
	}
	
	def subPackage(String suffix) {
		getBasePackage() + suffix.withDotPrefix
	}
	
	def private withDotPrefix(String name) {
		switch it: name {
			case nullOrEmpty:     ''
			case startsWith('.'): name
			default:              '.' + name
		}
	}
	
}
