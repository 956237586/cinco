package de.jabc.cinco.meta.core.utils.messages;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class CincoMessageHandler {
	
	private static Display getDisplay() {
		var display = Display.getCurrent();
		return display != null ? display : Display.getDefault();
	}
	
	public static void showMessage(String message, String title) {
		Display display = getDisplay();
		if (display != null) {
			display.syncExec(() -> MessageDialog.openInformation(display.getActiveShell(), title, message));	
		}
		else {
			System.out.println(message);
		}
		
	}
	
	public static void showErrorMessage(String message, String title) {
		Display display = getDisplay();
		if (display != null) {
			display.syncExec(() -> MessageDialog.openError(display.getActiveShell(), title, message));
		}
		else {
			System.err.println(message);
		}
	}
	
	public static boolean showQuestion(String question, String title, boolean defaultValue) {
		Display display = getDisplay();
		if (display != null) {
			return MessageDialog.openQuestion(display.getActiveShell(), title, question);
		}
		else {
			return defaultValue;
		}
	}
	
}