package de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker

import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableModel
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge

abstract class AbstractSubToSatModelChecker<M extends BasicCheckableModel> 
	extends AbstractBasicModelChecker<M> 
	implements SubToSatProvider<M, BasicCheckableNode, BasicCheckableEdge> {
}
