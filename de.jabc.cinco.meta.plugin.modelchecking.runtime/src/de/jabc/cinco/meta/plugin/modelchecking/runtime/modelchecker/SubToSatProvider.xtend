package de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker

import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import java.util.Set
import java.util.Map

interface SubToSatProvider<M extends CheckableModel<N, E>, N, E> {
	def Map<String, Set<N>> getSubToSatMap(M model, String expression) throws Exception
}