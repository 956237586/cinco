package de.jabc.cinco.meta.plugin.modelchecking.runtime.model

import graphmodel.Node
import java.util.Set

class ReplacementEdge {
	Node source
	Node target

	Set<String> labels = newHashSet

	new(Node source, Node target, Set<String> labels) {
		this.source = source
		this.target = target
		if (labels !== null) {
			this.labels.addAll(labels)
		}
	}
	
	def getSource(){
		source
	}
	
	def getTarget(){
		target
	}
	
	def setSource(Node source){
		this.source = source
	}
	
	def setTarget(Node target){
		this.target = target
	}
	
	def getLabels(){
		labels
	}
	
	def setLabels(Set<String> labels){
		this.labels = labels
	}
	
	def isValidEdge(){
		source !== null && target !== null
	}
}
