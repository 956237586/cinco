package de.jabc.cinco.meta.plugin.modelchecking.runtime.support

import org.eclipse.jface.viewers.EditingSupport
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension

abstract class ModelCheckingEditingSupport extends EditingSupport {
	
	protected val de.jabc.cinco.meta.plugin.modelchecking.runtime.views.ModelCheckingViewPart view
	protected FormulaHandler<?, ?> handler
	protected extension WorkbenchExtension = new WorkbenchExtension

	new(de.jabc.cinco.meta.plugin.modelchecking.runtime.views.ModelCheckingViewPart view) {
		super(view.viewer)
		this.view = view
	}

	def setFormulaHandler(FormulaHandler<?, ?> handler) {
		this.handler = handler
	}

	override protected canEdit(Object element) {
		true
	}
}
