package de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker

import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableModel

abstract class SimpleModelChecker extends AbstractBasicModelChecker<BasicCheckableModel> {
	override BasicCheckableModel createCheckableModel() {
		new BasicCheckableModel()
	}
}
