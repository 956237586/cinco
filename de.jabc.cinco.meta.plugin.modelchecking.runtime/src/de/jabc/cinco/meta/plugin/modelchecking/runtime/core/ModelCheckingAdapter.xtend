package de.jabc.cinco.meta.plugin.modelchecking.runtime.core

import java.util.Set
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import graphmodel.GraphModel
import graphmodel.Node
import de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight.HighlightDescription

interface ModelCheckingAdapter {
	def boolean canHandle(GraphModel model)

	def void buildCheckableModel(GraphModel model, CheckableModel<?, ?> checkableModel, boolean withSelection)

	def FormulaHandler<?, ?> getFormulaHandler()

	def Class<?> getFormulasTypeClass()
	
	def String getSettings(String key)

	def boolean fulfills(GraphModel model, CheckableModel<?, ?> checkableModel, Set<Node> satisfyingNodes)
	
	def Set<HighlightDescription> getHighlightDescriptions(GraphModel model, Set<HighlightDescription> intendedHighlightDescriptions)

}
