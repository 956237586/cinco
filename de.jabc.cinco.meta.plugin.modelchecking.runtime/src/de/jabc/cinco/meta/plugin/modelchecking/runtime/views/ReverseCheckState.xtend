package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import graphmodel.Node
import java.util.Set
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.Result
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel

class ReverseCheckState {
	
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension
	
	var Set<Node> selection
	var CheckableModel<?,?> checkableModel
	var containsInvalid = false
	
	def isEmpty(){
		selection.empty
	}
	
	def getSubToResultList(CheckFormula formula){
		val list = newLinkedList
		if (!empty && !containsInvalid){
			for (entry : formula.getSubToSatList){
				var result = Result.FALSE
				
				if (entry.value.containsAll(selection)){
					result = Result.TRUE
				}
				
				list.add(entry.key -> result)
			}
		}
		list
	}
	
	def satisfiedBySelection(CheckFormula formula){
		formula.satisfyingNodes.containsAll(selection)
	}
	
	def <N,E> setCheckableModel(CheckableModel<N,E> model){
		this.checkableModel = model
	}
	
	def updateSelection(){
		selection = activeModel.allNodesAndContainers.filter[selectedElementIds.contains(it.id)].toSet
		updateInvalid
	}
	
	def containsInvalidNodes(){
		containsInvalid
	}
	
	private def <N,E> updateInvalid(){
		containsInvalid = selection.exists[
			val checkableModel = checkableModel as CheckableModel<N,E>
			!checkableModel.nodes.map[checkableModel.getId(it)].toSet.contains(it.id)
		]
	}
}