package de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker

import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableModel
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge

abstract class SubToSatModelChecker extends SimpleModelChecker implements SubToSatProvider<BasicCheckableModel, BasicCheckableNode, BasicCheckableEdge>{
	
}
