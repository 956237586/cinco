package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.swt.SWT
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.Result
import org.eclipse.swt.custom.StyleRange
import java.util.Set

class StyleRangeProvider {
	extension WorkbenchExtension = new WorkbenchExtension

	val BLACK = display.getSystemColor(SWT.COLOR_BLACK)
	val RED = display.getSystemColor(SWT.COLOR_RED)
	val BLUE = display.getSystemColor(SWT.COLOR_BLUE)

	def getStyleRanges(CheckFormula formula, Set<String> varNames, boolean highlightUnSatSub) {
		val ranges = newLinkedList
		ranges.add(new StyleRange(0, formula.expression.length, BLACK, null))

		if (highlightUnSatSub) {
			if (formula.result == Result.FALSE) {
				ranges.add(new StyleRange(0, formula.expression.length, RED, null))
			}
		
			for (entry : formula.subToResultList) {
				val indices = getAllStartIndices(formula.expression, entry.key)
				if (entry.value == Result.TRUE) {
					for (index : indices) {
						ranges.add(new StyleRange(index, entry.key.length, BLACK, null))
					}
				} else {
					for (index : indices) {
						ranges.add(new StyleRange(index, entry.key.length, RED, null))
					}
				}
			}
		}

		ranges.addAll(formula.expression.getVarNameStyleRanges(varNames))

		ranges
	}

	def getStyleRanges(CheckFormula formula, Set<String> varNames, ReverseCheckState reverseCheckState,
		boolean highlightUnSatSub) {

		val ranges = newLinkedList
		ranges.add(new StyleRange(0, formula.expression.length, BLACK, null))

		if (highlightUnSatSub) {
			for (entry : reverseCheckState.getSubToResultList(formula)) {
				val indices = getAllStartIndices(formula.expression, entry.key)
				if (entry.value == Result.TRUE) {
					for (index : indices) {
						ranges.add(new StyleRange(index, entry.key.length, BLACK, null))
					}
				} else {
					for (index : indices) {
						ranges.add(new StyleRange(index, entry.key.length, RED, null))
					}
				}
			}

		}

		ranges.addAll(formula.expression.getVarNameStyleRanges(varNames))

		ranges
	}

	def getVarNameStyleRanges(String text, Set<String> varNames) {
		val ranges = newLinkedList
		for (varName : varNames) {
			val indices = getAllStartIndices(text, varName)
			for (index : indices) {
				ranges.add(new StyleRange(index, varName.length, BLUE, null))
			}
		}

		ranges.sortBy[start]
	}

	def getAllStartIndices(String expression, String subexpression) {
		val indices = newLinkedList
		var index = -1

		do {
			index++
			index = expression.indexOf(subexpression, index)

			if (index != -1) {
				indices.add(index)
			}
		} while (index != -1)

		indices
	}
}
