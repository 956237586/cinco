package de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight

import java.util.Set
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlight
import java.util.Map
import org.eclipse.swt.graphics.RGB
import graphmodel.Node
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.graphiti.util.IColorConstant
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension

class HighlightManager {
	
	extension WorkbenchExtension = new WorkbenchExtension
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension
	
	Set<HighlightDescription> currentHighlightDescriptions = newHashSet
	Set<Highlight> currentHighlights = newHashSet
	
	def setNewHighlightDescriptions(Map<RGB, Set<Node>> rgbMap) {

		val allNodes = activeGraphModel.allNodesAndContainers
		val intendedHighlightDescriptions = newHashSet

		for (entry : rgbMap.entrySet){
			val hd = new HighlightDescription => [
				foregroundColor = IColorConstant.BLACK
				backgroundColor = entry.key
				entry.value
					.filterNull
					.filter[allNodes.contains(it)]
					.forEach[node|
						add(node)
					]
			]
			intendedHighlightDescriptions.add(hd)
		}
		currentHighlightDescriptions = activeAdapter.getHighlightDescriptions(activeGraphModel, intendedHighlightDescriptions)		
	}

	def clearHighlight() {
		highlightOff
		currentHighlights = newHashSet
		currentHighlightDescriptions = newHashSet
	}

	def void highlightOn() {
		highlightOff
		currentHighlights = newHashSet
		currentHighlightDescriptions.forEach[
			val highlight = newHighlight
			currentHighlights.add(highlight)
			startFunction.accept(highlight)
		]
	}

	def void highlightOff() {
		currentHighlights.forEach[
			off
		]
	}
}