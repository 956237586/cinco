package de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas

import java.util.List
import graphmodel.GraphModel
import graphmodel.Type

interface FormulaFactory<M extends GraphModel, T extends Type> {
	
	def List<CheckFormula> createFormulas(M model)

	def CheckFormula createFormula(T formula)

	def T createGraphModelFormula(String exp, String varName)

}
