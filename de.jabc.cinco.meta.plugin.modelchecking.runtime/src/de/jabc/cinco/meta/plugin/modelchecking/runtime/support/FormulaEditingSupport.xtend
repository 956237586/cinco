package de.jabc.cinco.meta.plugin.modelchecking.runtime.support

import org.eclipse.jface.viewers.CellEditor
import org.eclipse.jface.viewers.TextCellEditor
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler.FormulaException

class FormulaEditingSupport extends ModelCheckingEditingSupport {

	final CellEditor editor

	new(de.jabc.cinco.meta.plugin.modelchecking.runtime.views.ModelCheckingViewPart view) {
		super(view)
		this.editor = new TextCellEditor(view.viewer.table)
	}

	override protected getCellEditor(Object element) {
		editor
	}

	override protected getValue(Object element) {
		(element as CheckFormula).expression
	}

	override protected setValue(Object element, Object value) {
		var newFormula = (value as String)
		var formula = (element as CheckFormula)
		if (handler !== null && newFormula != formula.expression) {
			try {
				handler.setExpression(formula, newFormula)
			} catch (FormulaException e) {
				var message = ""
				switch e.errorCode {
					case INVALID: message = "Expression must not be empty."
					case EXISTS: message = "Expression already exists."
					case CYCLE: message = "Expression contains circular dependencies."
				}
				showErrorDialog("Invalid expression", message)
			}finally{
				view.refreshAll
				view.autoCheckAllFormulas
			}			
		}
	}
}
