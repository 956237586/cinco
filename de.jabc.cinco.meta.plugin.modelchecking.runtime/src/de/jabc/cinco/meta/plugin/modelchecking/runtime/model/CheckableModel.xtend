package de.jabc.cinco.meta.plugin.modelchecking.runtime.model

import java.util.Set

interface CheckableModel<N,E> {
	def Set<N> getNodes()
	def Set<E> getEdges()
	def String getId(N node)
	def boolean isStartNode(N node)
	def void addNewNode(String id, boolean isStartNode, Set<String> atomicPropositions)
	def void addNewEdge(N source, N target, Set<String> labels)
}