package de.jabc.cinco.meta.plugin.modelchecking.runtime.gear

import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode
import de.metaframe.gear.AssemblyLine
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.SubToSatProvider
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.AbstractSubToSatModelChecker

class GearModelChecker extends AbstractSubToSatModelChecker<GEARModel> implements SubToSatProvider<GEARModel, BasicCheckableNode, BasicCheckableEdge> {
	override getSatisfyingNodes(GEARModel model, String expression) throws Exception {
		val line = new AssemblyLine<BasicCheckableNode, BasicCheckableEdge>
		line.addDefaultMacros
		line.modelCheck(model, expression)
	}

	override getSubToSatMap(GEARModel model, String expression) throws Exception {
		val line = new AssemblyLine<BasicCheckableNode, BasicCheckableEdge>
		line.addDefaultMacros
		line.compilationEnabled = false
		line.modelCheckAll(model, expression)
	}

	override getName() {
		"GEAR"
	}

	override createCheckableModel() {
		new GEARModel()
	}
}
