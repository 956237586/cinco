package de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker

import java.util.Set
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.ModelComparator

interface ModelChecker<M extends CheckableModel<N, E>, N, E> {
	def Set<N> getSatisfyingNodes(M model, String expression) throws Exception

	def String getName()

	def M createCheckableModel()

	def ModelComparator<M> getComparator()
}
